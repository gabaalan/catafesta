<?php

// module/Admin/conﬁg/module.config.php:
return array(
    'controllers' => array( //add module controllers
        'invokables' => array(
            'Admin\Controller\Auth'				=> 'Admin\Controller\AuthController',
            'Admin\Controller\Index'			=> 'Admin\Controller\IndexController',
            'Admin\Controller\User'				=> 'Admin\Controller\UserController',
            'Admin\Controller\Profile'			=> 'Admin\Controller\ProfileController',
            'Admin\Controller\Configurations'	=> 'Admin\Controller\ConfigurationsController',
            'Admin\Controller\Depoimento'		=> 'Admin\Controller\DepoimentoController',
            'Admin\Controller\Musicas'			=> 'Admin\Controller\MusicasController',
            'Admin\Controller\Fotos'			=> 'Admin\Controller\FotosController',
            'Admin\Controller\Categoria'		=> 'Admin\Controller\CategoriaController',
            'Admin\Controller\Parceiros'		=> 'Admin\Controller\ParceirosController',
            'Admin\Controller\Instrumento'		=> 'Admin\Controller\InstrumentoController',
            'Admin\Controller\Banners'          => 'Admin\Controller\BannersController',
        ),
		
    ),

    'router' => array(
        'routes' => array(
            'admin' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/admin',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                        'module'        => 'admin'
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                        'child_routes' => array( //permite mandar dados pela url 
                            'wildcard' => array(
                                'type' => 'Wildcard'
                            ),
                        ),
                    ),
                ),
            ),
            'banners' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/admin/banners[/:action[/:id]][/:page]',
                    'constraints' => array(
                        'action'	=> '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'		=> '[0-9]*',
                        'page'		=> '[0-9]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller'    => 'Banners',
                        'action'        => 'index',
                        'module'        => 'admin',
                    ),
                ),
            ),
            'user' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/admin/user[/:action[/:id]][/:page]',
					'constraints' => array(
						'action'	=> '[a-zA-Z][a-zA-Z0-9_-]*',
						'id'		=> '[0-9]*',
						'page'		=> '[0-9]*',
					),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller'    => 'User',
                        'action'        => 'index',
                        'module'        => 'admin',
                    ),
                ),
            ),
            'depoimento' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/admin/depoimento[/:action[/:id[/:value]]][/:page]',
					'constraints' => array(
						'action'	=> '[a-zA-Z][a-zA-Z0-9_-]*',
						'id'		=> '[0-9]*',
						'page'		=> '[0-9]*',
						'value'		=> '[0-9]{1}',
					),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller'    => 'Depoimento',
                        'action'        => 'index',
                        'module'        => 'admin',
                    ),
                ),
            ),
            'instrumento' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/admin/instrumento[/:action[/:id]][/:page]',
					'constraints' => array(
						'action'	=> '[a-zA-Z][a-zA-Z0-9_-]*',
						'id'		=> '[0-9]*',
						'page'		=> '[0-9]*',
					),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller'    => 'Instrumento',
                        'action'        => 'index',
                        'module'        => 'admin',
                    ),
                ),
            ),
            'musicas' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/admin/musica[/:action[/:id]][/:page]',
					'constraints' => array(
						'action'	=> '[a-zA-Z][a-zA-Z0-9_-]*',
						'id'		=> '[0-9]*',
						'page'		=> '[0-9]*',
					),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller'    => 'Musicas',
                        'action'        => 'index',
                        'module'        => 'admin',
                    ),
                ),
            ),
            'parceiro' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/admin/parceiro[/:action[/:id]][/:page]',
					'constraints' => array(
						'action'	=> '[a-zA-Z][a-zA-Z0-9_-]*',
						'id'		=> '[0-9]*',
						'page'		=> '[0-9]*',
					),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller'    => 'Parceiros',
                        'action'        => 'index',
                        'module'        => 'admin',
                    ),
                ),
            ),
			'foto' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/admin/fotos[/:action[/:id]][/:page]',
					'constraints' => array(
						'action'	=> '[a-zA-Z][a-zA-Z0-9_-]*',
						'id'		=> '[0-9]*',
						'page'		=> '[0-9]*',
					),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller'    => 'Fotos',
                        'action'        => 'index',
                        'module'        => 'admin',
                    ),
                ),
            ),
            'categoria' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/admin/categoria[/:action[/:id]][/:page]',
					'constraints' => array(
						'action'	=> '[a-zA-Z][a-zA-Z0-9_-]*',
						'id'		=> '[0-9]*',
						'page'		=> '[0-9]*',
					),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller'    => 'Categoria',
                        'action'        => 'index',
                        'module'        => 'admin',
                    ),
                ),
            ),
            'configuration' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/admin/configurations[/:action[/:id]][/:page]',
					'constraints' => array(
						'action'	=> '[a-zA-Z][a-zA-Z0-9_-]*',
						'id'		=> '[0-9]*',
						'page'		=> '[0-9]*',
					),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller'    => 'Configurations',
                        'action'        => 'index',
                        'module'        => 'admin',
                    ),
                ),
            ),
            'profile' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/admin/profile[/:action]',
					'constraints' => array(
						'action'	=> '[a-zA-Z][a-zA-Z0-9_-]*',
					),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller'    => 'Profile',
                        'action'        => 'index',
                        'module'        => 'admin',
                    ),
                ),
            ),
            'auth' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/admin/auth',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller'    => 'Auth',
                        'action'        => 'login',
                        'module'        => 'admin',
                    ),
                ),
            ),
            'login' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/admin/login',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'module'        => 'admin',
                        'controller'    => 'Auth',
                        'action'        => 'index',
                    ),
                ),
            ),
            'logout' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/admin/logout',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'module'        => 'admin',
                        'controller'    => 'Auth',
                        'action'        => 'logout',
                    ),
                ),
            ),
            'forgot' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/admin/forgot',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'module'        => 'admin',
                        'controller'    => 'Auth',
                        'action'        => 'forgot',
                    ),
                ),
            ),
        ),
    ),
	
	'service_manager' => array(
		'factories' => array(
			'translator'			=> 'Zend\I18n\Translator\TranslatorServiceFactory',
			'Session'	=> function( $serviceManager )
			{
				return new Zend\Session\Container('Admin');
			},
			'Admin\Service\Auth'	=> function( $serviceManager )
			{
				$dbAdapter = $serviceManager->get('DbAdapter');
				return new Admin\Service\Auth( $dbAdapter );
			},
			'Db\Admin\Model\User'	=> function( $serviceManager )
			{
				$dbAdapter = $serviceManager->get('DbAdapter');

				$user = new Admin\Model\User();
				
				$user->setDbAdapter($dbAdapter);
				
				return $user;
			},
			'Db\Admin\Model\Musica'	=> function( $serviceManager )
			{
				$dbAdapter = $serviceManager->get('DbAdapter');

				$model = new Admin\Model\Musica();
				
				$model->setDbAdapter( $dbAdapter );
				
				return $model;
			},
			'Db\Admin\Model\Categoria'	=> function( $serviceManager )
			{
				$dbAdapter = $serviceManager->get('DbAdapter');

				$model = new Admin\Model\Categoria();
				
				$model->setDbAdapter( $dbAdapter );
				
				return $model;
			},
			'Db\Admin\Model\Profissional'	=> function( $serviceManager )
			{
				$dbAdapter = $serviceManager->get('DbAdapter');

				$model = new Admin\Model\Profissional();
				
				$model->setDbAdapter( $dbAdapter );
				
				return $model;
			},
			'admin'	=> 'Admin\Navigation\Admin',
		),
    ),
					
	'translator' => array(
		'locale' => 'pt_BR',
		'translation_file_patterns' => array(
			array(
				'type'     => 'phparray', //'gettext',
				'base_dir' => __DIR__ . '/../language',
				'pattern'  => '%s.php', //'%s.mo',
			),
		),
	),
					
	'view_helpers'	=> array(
		'invokables'	=> array(
		)
	),
	
	'view_manager' => array(
		'display_not_found_reason' => true,
		'display_exceptions'       => true,
		'doctype'                  => 'HTML5',
		'not_found_template'       => 'admin/error/404',
		'exception_template'       => 'admin/error/index',
		'template_map' => array(
			'layout/admin'			=> realpath( __DIR__ . '/../view/layout/layout.phtml' ),
			'layout/login'			=> realpath( __DIR__ . '/../view/layout/login.phtml' ),
			'admin/error/404'		=> realpath( __DIR__ . '/../view/error/404.phtml' ),
			'admin/error/index'		=> realpath( __DIR__ . '/../view/error/index.phtml' ),
		),
		'template_path_stack' => array(
			'admin' => __DIR__ . '/../view',
		),
		'strategies' => array(
            'ViewJsonStrategy',
        ),
	),
				
	'navigation'	=> array(
		'admin'	=> array(
			array(
				'label'		=> 'Depoimentos',
				'route'		=> 'depoimento',
				'controller'=> 'Depoimento',
				'action'	=> 'index',
			),
            array(
                'label'		=> 'Banners',
                'route'		=> 'banners',
                'controller'=> 'Banners',
                'action'	=> 'index',
            ),
			array(
				'label'		=> 'Fotos',
				'route'		=> 'foto',
				'controller'=> 'Fotos',
				
			),
			array(
				'label'		=> 'Músicas',
				'route'		=> 'musicas',
				'controller'=> 'Musicas',
				'action'	=> 'index',
			),
			array(
				'label'		=> 'Parceiros',
				'route'		=> 'parceiro',
				'controller'=> 'Parceiros',
				'action'	=> 'index',
			),
			array(
				'label'		=> 'Instrumentos',
				'route'		=> 'instrumento',
				'controller'=> 'Instrumento',
				'action'	=> 'index',
			),
			array(
				'label'	=> 'Administração',
				'route'	=> 'user',
				'controller'	=> 'User',
				'pages'	=> array(
					array(
						'label'			=> 'Categorias',
						'route'			=> 'categoria',
						'controller'	=> 'Categoria',
						'action'		=> 'index',
						'icon'			=> 'glyphicon-tags'
					),
					array(
						'label'	=> 'Usuários',
						'route'	=> 'user',
						'controller'	=> 'User',
						'action'	=> 'index',
						'icon'	=> 'glyphicon-user'
					),
					array(
						'label'	=> 'Configurações',
						'route'	=> 'configuration',
						'controller'	=> 'Configurations',
						'action'	=> 'index',
						'icon'	=> 'glyphicon-wrench'
					),
				)
			),
		)
	),
);