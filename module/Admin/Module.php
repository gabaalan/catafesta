<?php

namespace Admin;

use Zend\Mvc\ModuleRouteListener;
use Zend\Authentication\AuthenticationService;
use Zend\Mvc\MvcEvent;

class Module
{
	protected $_flashMessenger;
	
	/**
	 * Executada no bootstrap do módulo
	 * @param MvcEvent $event
	 */
	public function onBootstrap($event)
	{
		/** @var \Zend\ModuleManager\ModuleManager $moduleManager */
		$moduleManager = $event->getApplication()->getServiceManager()->get('modulemanager');
		
		$translator = $event->getApplication()->getServiceManager()->get('translator');
		
		\Zend\Validator\AbstractValidator::setDefaultTranslator( new \Zend\Mvc\I18n\Translator( $translator ) );
		
		$eventManager        = $event->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
		
		/** @var \Zend\EventManager\SharedEventManager $sharedEvents */
		$sharedEvents = $moduleManager->getEventManager()->getSharedManager();

		//adiciona eventos ao módulo - 'Zend\Mvc\Controller\AbstractActionController' <- Geral
		$sharedEvents->attach(__NAMESPACE__, MvcEvent::EVENT_DISPATCH, array($this, 'mvcPreDispatch'), 100);
		$sharedEvents->attach(__NAMESPACE__, MvcEvent::EVENT_DISPATCH, array($this, 'setLayout'), 99);
		$sharedEvents->attach(__NAMESPACE__, MvcEvent::EVENT_DISPATCH_ERROR, array($this, 'onDispatchError'), 99);
		
//		/$eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, array($this, 'onDispatchError'), 100);		
    }
	
	public function onDispatchError( MvcEvent $event )
	{
		if( !$event->getRouteMatch() )
		{
			$viewModel = $event->getViewModel();
			$viewModel->setTemplate('layout/login');
			
			return;
		}
		
		$module = $event->getRouteMatch()->getParam('module');
		
		if( $module === strtolower( __NAMESPACE__ ) )
		{
			$viewModel = $event->getViewModel();
			$viewModel->setTemplate('layout/admin');
		}
	}

	public function setLayout( $event )
	{
		$controller = $event->getTarget();
		$controller->layout('layout/admin');
	}
	
	/**
	 * Verifica se precisa fazer a autorização do acesso
	 * @param  MvcEvent $event Evento
	 * @return boolean
	 */
	public function mvcPreDispatch( MvcEvent $event )
	{
		$serviceLocator	= $event->getTarget()->getServiceLocator();
		$routeMatch		= $event->getRouteMatch();
		$moduleName		= $routeMatch->getParam('module');
		$controllerName = $routeMatch->getParam('controller');
		$actionName		= $routeMatch->getParam('action');

		$authService = $serviceLocator->get('Admin\Service\Auth');
		
		//if( !$authService->authorize( $moduleName, $controllerName, $actionName ) )
		if( $moduleName == 'admin' && $controllerName != 'Admin\Controller\Auth' )
		{
			//throw new \Exception('Você não tem permissão para acessar este recurso');
			
			$auth = $authService->getAuthenticationService( $authService::NAMESPACE_ADMIN );
			
			$redirect = $event->getTarget()->redirect();
			
			if( !$auth->hasIdentity() )
			{
				$serviceLocator
					->get('ControllerPluginManager')
					->get('flashMessenger')
					->addErrorMessage('Você precisa estar logado para acessar esta página');
				
				$event->setResponse( $redirect->toRoute('login') );
				//$event->stopPropagation();
				
				return false;
			}
		}
		
		return true;
	}
	
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
}