<?php

namespace Admin\Navigation;

use Zend\Navigation\Service\DefaultNavigationFactory;

class Admin extends DefaultNavigationFactory 
{
    protected function getName() 
	{
        return 'admin';
    }
}