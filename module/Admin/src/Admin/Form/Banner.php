<?php
namespace Admin\Form;
 
use Zend\Form\Form;
use Zend\Form\Element;

class Banner extends Form
{
    public function __construct()
    {
        parent::__construct('post');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form form-horizontal has-content-editable');
        
		// ------------------------------------------------------------------------
		
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));

		$path = new Element\File('path');
        $path
            ->setLabel('Imagem')
			->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
			->setAttributes(array(
				'class'	=> 'span5 form-control'
			));
		
		$description = new Element\Textarea('descricao');
		$description
			->setLabel('Descrição')
			->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
			->setAttributes(array(
				'class'	=> 'span5 form-control editor'
			));


        $limite = new Element\Text('limite');
        $limite
            ->setLabel('Data final')
            ->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
            ->setAttributes(array(
                'size'  => '10',
                'placeholder'	=> '01/01/' . date('Y'),
                'class'	=> 'span2 form-control date'
            ));

        $hora = new Element\Text('hora');
        $hora
            ->setLabel('Hora final')
            ->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
            ->setAttributes(array(
                'size'  => '10',
                'placeholder'	=> '00:00',
                'class'	=> 'span2 form-control hour'
            ));

		$this->add( $path );
        $this->add( $limite );
        $this->add( $hora );
		$this->add( $description );

		// ------------------------------------------------------------------------
		
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Salvar',
                'class'  => 'btn btn-primary',
                'id' => 'submitbutton',
            ),
        ));
		
		$this->add(array(
            'name' => 'reset',
            'attributes' => array(
                'type'  => 'reset',
                'value' => 'Cancelar',
                'class'  => 'btn',
				'onClick'	=> "window.location='/admin/user'",
                'id' => 'resetbutton',
            ),
        ));
    }
	
	public function bind( $object, $flags = \Zend\Form\FormInterface::VALUES_NORMALIZED ) 
	{
        if( $object->limite )
        {
            $date = \DateTime::createFromFormat( 'Y-m-d H:i:s', $object->limite );

            $object->limite = $date->format( 'd/m/Y' );

            $object->hora = $date->format( 'H:i:s' );
        }

		parent::bind( $object, $flags );
	}
	
	public function setData( $data ) 
	{		
		if( isset( $data['submit'] ) )
			unset( $data['submit'] );

        if( ( isset( $data['limite'] ) && isset( $data['hora'] ) ) && ( !empty( $data['limite'] ) && !empty( $data['hora'] ) ) )
        {
            $date = \DateTime::createFromFormat( 'd/m/Y H:i', $data['limite'] . ' ' . $data['hora'] );

            $data['limite'] = $date->format( 'd/m/Y H:i:s' );
        }

		parent::setData($data);
	}
}