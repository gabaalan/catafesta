<?php
namespace Admin\Form;
 
use Zend\Form\Form;
use Zend\Form\Element;

use Zend\Db\Sql\Sql;

class Musica extends Form
{
    public function __construct( $dbAdapter, $options = array() )
    {
        parent::__construct('post');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form form-horizontal');
        $this->setAttribute('action', '/admin/musica/save');
        
		// ------------------------------------------------------------------------
		
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));

		$nome = new Element\Text('nome');
		$nome
			->setLabel('Nome')
			->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
			->setAttributes(array(
				'size'		=> '200',
				'class'		=> 'form-control',
				'required'	=> 'true',
			));
		
		$autor = new Element\Text('autor');
		$autor
			->setLabel('Autor')
			->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
			->setAttributes(array(
				'size'		=> '200',
				'class'		=> 'form-control',
				'required'	=> 'true',
			));
		
		$tipo = new Element\Text('tipo');
		$tipo
			->setLabel('Tipo')
			->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
			->setAttributes(array(
				'size'		=> '200',
				'class'		=> 'form-control',
				'required'	=> 'true',
			));
		
		$musica = new Element\File('caminho');
        $musica
            ->setLabel('Música')
			->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
			->setAttributes(array(
				'class'	=> 'span5 form-control'
			));

		// ------------------------------------------------------------------------
		
		$categoria = new Element\Select('categoria');
		$categoria	->setLabel('Categoria')
				->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
				->setAttributes(array(
					'class'		=> 'form-control',
					'required'	=> 'true',
				));

		$sql = new Sql( $dbAdapter );

		$select = $sql->select();

		$select	->columns(array( 'id', 'nome' ))
				->from( 'categorias' )
				->where( array( 'tipo_id' => 1 ) )
				->order('nome ASC')
				;

		$statement = $sql->prepareStatementForSqlObject( $select );

		$role_options = array();
		
		foreach( $statement->execute() as $registro )
		{
			$role_options[ $registro['id'] ] = $registro['nome'];
		}
		
		$categoria->setValueOptions( $role_options );
		
		// ------------------------------------------------------------------------
		
		$this->add( $nome );
		$this->add( $autor );
		$this->add( $tipo );
		$this->add( $musica );
		$this->add( $categoria );

		// ------------------------------------------------------------------------
		
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Salvar',
                'class'  => 'btn btn-primary',
                'id' => 'submitbutton',
            ),
        ));
		
        $this->add(array(
            'name' => 'reset',
            'attributes' => array(
                'type'		=> 'reset',
                'value'		=> 'Cancelar',
                'class'		=> 'btn',
				'onClick'	=> "window.location='/musica'",
                'id'		=> 'submitbutton',
            ),
        ));
    }
	
	public function bind( $object, $flags = \Zend\Form\FormInterface::VALUES_NORMALIZED ) 
	{
		parent::bind( $object, $flags );
	}
	
	public function setData( $data ) 
	{		
		if( isset( $data['submit'] ) )
			unset( $data['submit'] );
		
		parent::setData($data);
	}
}