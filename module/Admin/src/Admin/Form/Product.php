<?php
namespace Admin\Form;
 
use Zend\Form\Form;
use Zend\Form\Element;

use Zend\Db\Sql\Sql;

class Product extends Form
{
    public function __construct( $dbAdapter, $options = array() )
    {
        parent::__construct('post');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form form-horizontal');
        $this->setAttribute('action', '/product/save');
        
		// ------------------------------------------------------------------------
		
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));

		$name = new Element\Text('name');
		$name
			->setLabel('Nome')
			->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
			->setAttributes(array(
				'size'		=> '200',
				'class'		=> 'form-control',
				'required'	=> 'true',
			));
		
		$value = new Element\Text('value');
		$value
			->setLabel('Valor do produto')
			->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
			->setAttributes(array(
				'size'		=> '20',
				'class'		=> 'form-control',
				'required'	=> 'true',
			));

		// ------------------------------------------------------------------------
		
		$type = new Element\Select('type_id');
		$type	->setLabel('Tipo do produto')
				->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
				->setAttributes(array(
					'class'		=> 'form-control',
					'required'	=> 'true',
				));

		$sql = new Sql( $dbAdapter );

		$select = $sql->select();

		$select	->columns(array( 'id', 'name' ))
				->from( 'types' )
				->order('name ASC')
				;

		$statement = $sql->prepareStatementForSqlObject( $select );

		$role_options = array();
		
		foreach( $statement->execute() as $registro )
		{
			$role_options[ $registro['id'] ] = $registro['name'];
		}
		
		$type->setValueOptions( $role_options );
		
		// ------------------------------------------------------------------------
		
		$this->add( $name );
		$this->add( $value );
		$this->add( $type );

		// ------------------------------------------------------------------------
		
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Salvar',
                'class'  => 'btn btn-primary',
                'id' => 'submitbutton',
            ),
        ));
		
        $this->add(array(
            'name' => 'reset',
            'attributes' => array(
                'type'		=> 'reset',
                'value'		=> 'Cancelar',
                'class'		=> 'btn',
				'onClick'	=> "window.location='/products'",
                'id'		=> 'submitbutton',
            ),
        ));
    }
	
	public function bind( $object, $flags = \Zend\Form\FormInterface::VALUES_NORMALIZED ) 
	{
		if( $object->value )
		{
			$object->value = number_format( $object->value, 2, ',', '.' );
		}
		
		parent::bind( $object, $flags );
	}
	
	public function setData( $data ) 
	{		
		if( isset( $data['submit'] ) )
			unset( $data['submit'] );
		
		parent::setData($data);
	}
}