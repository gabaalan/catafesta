<?php
namespace Admin\Form;
 
use Zend\Form\Form;
 
class Forgot extends Form
{
    public function __construct()
    {
        parent::__construct('forgot');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form');

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'  => 'text',
				'class'  => 'form-control input-large',	
				'required'	=> 'true'
            ),
            'options' => array(
                'label' => 'E-mail',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Enviar senha',
				'class'  => 'btnLogin btn btn-info pull-right',
                'id' => 'submitbutton',
            ),
        ));
    }
}