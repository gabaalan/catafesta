<?php
namespace Admin\Form;
 
use Zend\Form\Form;
use Zend\Form\Element;

use Zend\Db\Sql\Sql;

class Profissional extends Form
{
    public function __construct( $dbAdapter, $options = array() )
    {
        parent::__construct('post');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form form-horizontal');
        $this->setAttribute('action', '/admin/parceiro/save');
        
		// ------------------------------------------------------------------------
		
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));

		$nome = new Element\Text('nome');
		$nome
			->setLabel('Nome')
			->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
			->setAttributes(array(
				'size'		=> '200',
				'class'		=> 'form-control',
				'required'	=> 'true',
			));
		
		$telefone = new Element\Text('telefone');
		$telefone
			->setLabel('Telefone')
			->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
			->setAttributes(array(
				'size'		=> '200',
				'class'		=> 'form-control',
				'required'	=> 'true',
			));
		
		$email = new Element\Text('email');
		$email
			->setLabel('E-mail')
			->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
			->setAttributes(array(
				'size'		=> '200',
				'class'		=> 'form-control',
			));
		
		$site = new Element\Text('site');
		$site
			->setLabel('Site')
			->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
			->setAttributes(array(
				'size'		=> '200',
				'class'		=> 'form-control',
			));
		
		$descricao = new Element\Text('descricao');
		$descricao
			->setLabel('Descrição')
			->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
			->setAttributes(array(
				'size'		=> '200',
				'class'		=> 'form-control',
			));

		// ------------------------------------------------------------------------
		
		$categoria = new Element\Select('categoria_id');
		$categoria	->setLabel('Categoria')
				->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
				->setAttributes(array(
					'class'		=> 'form-control',
					'required'	=> 'true',
				));

		$sql = new Sql( $dbAdapter );

		$select = $sql->select();

		$select	->columns(array( 'id', 'nome' ))
				->from( 'categorias' )
				->where( array( 'tipo_id' => 2 ) )
				->order('nome ASC')
				;

		$statement = $sql->prepareStatementForSqlObject( $select );

		$role_options = array();
		
		foreach( $statement->execute() as $registro )
		{
			$role_options[ $registro['id'] ] = $registro['nome'];
		}
		
		$categoria->setValueOptions( $role_options );
		
		// ------------------------------------------------------------------------
		
		$this->add( $nome );
		$this->add( $telefone );
		$this->add( $email );
		$this->add( $site );
		$this->add( $descricao );
		$this->add( $categoria );

		// ------------------------------------------------------------------------
		
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Salvar',
                'class'  => 'btn btn-primary',
                'id' => 'submitbutton',
            ),
        ));
		
        $this->add(array(
            'name' => 'reset',
            'attributes' => array(
                'type'		=> 'reset',
                'value'		=> 'Cancelar',
                'class'		=> 'btn',
				'onClick'	=> "window.location='/musica'",
                'id'		=> 'submitbutton',
            ),
        ));
    }
	
	public function bind( $object, $flags = \Zend\Form\FormInterface::VALUES_NORMALIZED ) 
	{
		parent::bind( $object, $flags );
	}
	
	public function setData( $data ) 
	{		
		if( isset( $data['submit'] ) )
			unset( $data['submit'] );
		
		parent::setData($data);
	}
}