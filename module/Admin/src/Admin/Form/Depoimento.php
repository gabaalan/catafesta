<?php
namespace Admin\Form;
 
use Zend\Form\Form;
use Zend\Form\Element;

class Depoimento extends Form
{
    public function __construct()
    {
        parent::__construct('post');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form form-horizontal has-content-editable');
        
		// ------------------------------------------------------------------------
		
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));

		$nome = new Element\Text('nome');
		$nome
			->setLabel('Nome')
			->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
			->setAttributes(array(
				'size'  => '255',
				'class'	=> 'span5 form-control',
				'required'	=> 'true',
			));

		$foto = new Element\File('foto');
        $foto
            ->setLabel('Foto')
			->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
			->setAttributes(array(
				'class'	=> 'span5 form-control'
			));
		
		$aprovado = new Element\Radio('aprovado');
		$aprovado->setLabel('Aprovado?')
				->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
				->setValue('0')
				->setValueOptions(array(
					array( 'value' => '1', 'label' => 'Sim', 'label_attributes' => array('class' => 'radio inline'), ),
					array( 'value' => '0', 'label' => 'Não', 'label_attributes' => array('class' => 'radio inline'), ),
				));
		
		$email = new Element\Text('email');
		$email
			->setLabel('E-mail')
			->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
			->setAttributes(array(
				'size'  => '100',
				'required'	=> 'true',
				'class'	=> 'form-control'
			));
		
		$mensagem = new Element\Textarea('mensagem');
		$mensagem
			->setLabel('Mensagem')
			->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
			->setAttributes(array(
				'class'	=> 'span5 form-control editor'
			));
		
		$this->add( $nome );
		$this->add( $email );
		$this->add( $foto );
		$this->add( $aprovado );
		$this->add( $mensagem );

		// ------------------------------------------------------------------------
		
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Salvar',
                'class'  => 'btn btn-primary',
                'id' => 'submitbutton',
            ),
        ));
		
		$this->add(array(
            'name' => 'reset',
            'attributes' => array(
                'type'  => 'reset',
                'value' => 'Cancelar',
                'class'  => 'btn',
				'onClick'	=> "window.location='/admin/user'",
                'id' => 'submitbutton',
            ),
        ));
    }
	
	public function bind( $object, $flags = \Zend\Form\FormInterface::VALUES_NORMALIZED ) 
	{
		$object->aprovado = $object->aprovado ? '1' : '0';
		
		parent::bind( $object, $flags );
	}
	
	public function setData( $data ) 
	{		
		if( isset( $data['submit'] ) )
			unset( $data['submit'] );
		
		parent::setData($data);
	}
}