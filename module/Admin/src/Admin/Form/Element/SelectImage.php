<?php

namespace Admin\Form\Element;

use Zend\Form\Element;

class SelectImage extends Element
{
	protected $attributes = array(
		'type'	=> 'selectimage'
	);
}