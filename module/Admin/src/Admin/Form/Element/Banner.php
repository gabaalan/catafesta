<?php

namespace Admin\Form\Element;

use Zend\Form\Element;

class Banner extends Element
{
	protected $attributes = array(
		'type'	=> 'banner'
	);
}