<?php

namespace Admin\Form\View\Helper;

use Admin\Form\Element\SelectImage;
use Admin\Form\Element\Banner;
use Zend\Form\View\Helper\FormElement as BaseFormElement;
use Zend\Form\ElementInterface;

class FormElement extends BaseFormElement
{
	public function render( ElementInterface $element )
    {
		$renderer = $this->getView();
		
		if( !method_exists( $renderer, 'plugin' ) )
		{
			// Bail early if renderer is not pluggable
			return '';
		}
		
		if( $element instanceof SelectImage )
		{
			$helper = $renderer->plugin('form_select_image');
			return $helper( $element );
		}
		
		if( $element instanceof Banner )
		{
			$helper = $renderer->plugin('form_banner');
			return $helper( $element );
		}
		
		return parent::render($element);
    }
}