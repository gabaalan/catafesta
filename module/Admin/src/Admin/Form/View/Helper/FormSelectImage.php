<?php

namespace Admin\Form\View\Helper;

use Zend\Form\View\Helper\AbstractHelper;
use Zend\Form\ElementInterface;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Predicate;

class FormSelectImage extends AbstractHelper
{
	public function render( ElementInterface $element )
	{
		$value = $element->getValue();

		$elements = '<ul class="thumbnails">';
		
		if( count( $value ) )
		{	
			$dbAdapter = $this->getView()->service()->get('DbAdapter');

			$sql = new Sql( $dbAdapter );

			$predicate = new Predicate();
			$predicate->in('id', $value);

			$select = $sql->select();

			$select	->columns( array( 'id', 'path' ) )
					->from( 'images' )
					->where( $predicate )
					->order('id DESC')
					;

			$paths = $sql->prepareStatementForSqlObject( $select )->execute();

			foreach( $paths as $path )
			{
				$elements .= '<li class="span1 image">
					<input type="checkbox" checked="checked" value="' . $path['id'] . '" name="images[]">
					<a rel="shadowbox" class="thumbnail" href="' . $this->getView()->basePath( 'upload/images/' . $path['path'] ) . '">
						<img alt="" src="' . $this->getView()->basePath( 'upload/images/small_' . $path['path'] ) . '">
					</a>
				</li>';
			}
		}
		
		$elements .= '</ul>';
			
		$button = '<a href="javascript:;" class="btn btn-info select-image" data-target="' . $element->getName() . '">Selecionar</a>';
		
		return '<div class="super-select">' . $elements . $button . '</div>';
	}
	
	public function __invoke( ElementInterface $element = null )
	{
		return $this->render($element);
	}
}