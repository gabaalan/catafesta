<?php

namespace Admin\Form\View\Helper;

use Zend\Form\View\Helper\AbstractHelper;
use Zend\Form\ElementInterface;

class FormBanner extends AbstractHelper
{
	public function render( ElementInterface $element )
	{
		$value = htmlentities( addcslashes( ( $element->getValue() ), '\\\"\'/') );
		
		$hidden = '<input type="hidden" id="' . $element->getName() . '" name="' . $element->getName() . '" value="' . $value . '" />';
		
		$buttons = '<div class="btn-toolbar banner-edit-buttons">';
		
		$buttons .= '<div class="btn-group">';		
		$buttons .= '<a href="javascript:;" class="btn btn-mini" id="bold"><i class="icon-bold"></i></a>';
		$buttons .= '<a href="javascript:;" class="btn btn-mini" id="italic"><i class="icon-italic"></i></a>';
		$buttons .= '<a href="javascript:;" class="btn btn-mini" id="underline"><i class="icon-text-width"></i></a>';
		$buttons .= '</div>';
		
		$buttons .= '<div class="btn-group">';		
		$buttons .= '<a href="javascript:;" class="btn btn-mini" id="paragraph" title="Adicionar parágrafo"><i class="icon-align-justify"></i></a>';
		$buttons .= '<a href="javascript:;" class="btn btn-mini" id="title" title="Adicionar Título"><i class="icon-font"></i></a>';
		$buttons .= '<a href="javascript:;" class="btn btn-mini" id="block" title="Adicionar bloco"><i class="icon-stop"></i></a>';
		$buttons .= '<a href="javascript:;" class="btn btn-mini" id="remove" title="Remover elemento"><i class="icon-trash"></i></a>';
		$buttons .= '</div>';
		
		$buttons .= '<div class="btn-group">';		
		$buttons .= '<a href="javascript:;" class="btn btn-mini" id="indexup" title="Passar para cima"><i class="icon-arrow-up"></i></a>';
		$buttons .= '<a href="javascript:;" class="btn btn-mini" id="indexdown" title="Passar para baixo"><i class="icon-arrow-down"></i></a>';
		$buttons .= '</div>';
		
		$buttons .= '<div class="btn-group">';		
		$buttons .= '<a href="javascript:;" class="btn btn-link btn-mini">Fonte</a>';		
		$buttons .= '<select name="fontfamily" id="fontfamily">';
		$buttons .= '<option value="Arial" style="font-family: Arial;">Arial</option>';
		$buttons .= '<option value="Georgia" style="font-family: Georgia;">Georgia</option>';
		$buttons .= '<option value="colaborate-boldregular" style="font-family: colaborate-boldregular;">Colaborate Bold</option>';
		$buttons .= '<option value="colaborate-mediumregular" style="font-family: colaborate-mediumregular;">Colaborate Medium</option>';
		$buttons .= '<option value="colaborate-regularregular" style="font-family: colaborate-regularregular;">Colaborate Regular</option>';
		$buttons .= '<option value="colaboratelightregular" style="font-family: colaboratelightregular;">Colaborate Light</option>';
		$buttons .= '<option value="colaborate-thinregular" style="font-family: colaborate-thinregular;">Colaborate Thin</option>';
		$buttons .= '<option value="yanone_kaffeesatzbold" style="font-family: yanone_kaffeesatzbold;">Yanone Bold</option>';
		$buttons .= '<option value="yanone_kaffeesatz_regularRg" style="font-family: yanone_kaffeesatz_regularRg;">Yanone Regular</option>';
		$buttons .= '<option value="yanone_kaffeesatzlight" style="font-family: yanone_kaffeesatzlight;">Yanone Light</option>';
		$buttons .= '<option value="yanone_kaffeesatzthin" style="font-family: yanone_kaffeesatzthin;">Yanone Thin</option>';
		$buttons .= '<option value="great_vibesregular" style="font-family: great_vibesregular;">Great Vibes</option>';
		$buttons .= '</select>';
		$buttons .= '<a href="javascript:;" class="btn btn-link btn-mini">Cor de fundo</a>';	
		$buttons .= '<input type="color" name="backcolor" id="backcolor" class="span1" />';
		$buttons .= '<a href="javascript:;" class="btn btn-link btn-mini">Cor da fonte</a>';	
		$buttons .= '<input type="color" name="fontcolor" id="fontcolor" class="span1" />';
		$buttons .= '</div>';
		
		$buttons .= '</div>';
		
		return $hidden . $buttons . '<div class="banner-edit" id="banner-edit" data-target="' . $element->getName() . '">' . $element->getValue() . '</div>';
	}
	
	public function __invoke( ElementInterface $element = null )
	{
		return $this->render($element);
	}
}