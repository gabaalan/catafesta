<?php
namespace Admin\Form;
 
use Zend\Form\Form;
use Zend\Form\Element;

class Musico extends Form
{
    public function __construct( $options = array() )
    {
        parent::__construct('post');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form form-horizontal');
        $this->setAttribute('action', '/admin/instrumento/save');
        
		// ------------------------------------------------------------------------
		
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));

		$name = new Element\Text('nome');
		$name
			->setLabel('Nome')
			->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
			->setAttributes(array(
				'size'		=> '200',
				'class'		=> 'form-control',
				'required'	=> 'true',
			));
		
		// ----------------------------------------------------------------------------------------
		
		$this->add( $name );

		// ------------------------------------------------------------------------
		
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Salvar',
                'class'  => 'btn btn-primary',
                'id' => 'submitbutton',
            ),
        ));
		
        $this->add(array(
            'name' => 'reset',
            'attributes' => array(
                'type'		=> 'reset',
                'value'		=> 'Cancelar',
                'class'		=> 'btn',
				'onClick'	=> "window.location='/products'",
                'id'		=> 'submitbutton',
            ),
        ));
    }
	
	public function bind( $object, $flags = \Zend\Form\FormInterface::VALUES_NORMALIZED ) 
	{		
		parent::bind( $object, $flags );
	}
	
	public function setData( $data ) 
	{		
		if( isset( $data['submit'] ) )
			unset( $data['submit'] );
		
		parent::setData($data);
	}
}