<?php
namespace Admin\Form;
 
use Zend\Form\Form;
use Zend\Form\Element;

use Core\Helper\Hash;

class User extends Form
{
    public function __construct( $dbAdapter, $options = array() )
    {
        parent::__construct('post');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form form-horizontal');
        $this->setAttribute('action', '/user/save');
        
		// ------------------------------------------------------------------------
		
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));

		$name = new Element\Text('name');
		$name
			->setLabel('Nome')
			->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
			->setAttributes(array(
				'size'  => '255',
				'class'	=> 'form-control',
				'required'	=> 'true',
			));
		
		$email = new Element\Text('email');
		$email
			->setLabel('E-mail')
			->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
			->setAttributes(array(
				'size'  => '100',
				'required'	=> 'true',
				'class'	=> 'form-control'
			));
		
		$username = new Element\Text('username');
		$username
			->setLabel('Username')
			->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
			->setAttributes(array(
				'class' => 'form-control username',
				'required'	=> 'true',
				'size'  => '100',
			));

		$password = new Element\Password('password');
		$password
			->setLabel('Password')
			->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
			->setAttributes(array(
				'size'  => '30',
				'class'  => 'form-control',
			));
		
		$approved = new Element\Radio('valid');
		$approved->setLabel('Ativado?')
				->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
				->setValue('true')
				->setValueOptions(array(
					array( 'value' => '1', 'label' => 'Sim', 'label_attributes' => array('class' => 'radio inline'), ),
					array( 'value' => '0', 'label' => 'Não', 'label_attributes' => array('class' => 'radio inline'), ),
				));

		// ------------------------------------------------------------------------
		
		$this->add( $name );
		$this->add( $email );
		$this->add( $username );
		$this->add( $password );
		$this->add( $approved );

		// ------------------------------------------------------------------------
		
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Salvar',
                'class'  => 'btn btn-primary',
                'id' => 'submitbutton',
            ),
        ));
		
        $this->add(array(
            'name' => 'reset',
            'attributes' => array(
                'type'  => 'reset',
                'value' => 'Cancelar',
                'class'  => 'btn',
				'onClick'	=> "window.location='/admin/user'",
                'id' => 'submitbutton',
            ),
        ));
    }
	
	public function bind( $object, $flags = \Zend\Form\FormInterface::VALUES_NORMALIZED ) 
	{		
		$object->valid = $object->valid ? '1' : '0';
		
		parent::bind( $object, $flags );
	}
	
	public function setData( $data ) 
	{		
		if( isset( $data['submit'] ) )
			unset( $data['submit'] );
		
		parent::setData($data);
	}
}