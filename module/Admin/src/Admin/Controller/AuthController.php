<?php
namespace Admin\Controller;
 
use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Admin\Form\Login;
use Admin\Form\Forgot;

use Core\Helper\Hash;

use Zend\Authentication\AuthenticationService;
 
/**
 * Controlador que gerencia os posts
 * 
 * @category Admin
 * @package Controller
 * @author  Gaba
 */
class AuthController extends ActionController
{	
    /**
     * Mostra o formulário de recuperação de senha
     * @return void
     */
    public function forgotAction()
    {
		$this->layout('layout/login');
		
		$form = new Forgot();
		
		$request = $this->getRequest();
		
		if( $request->isPost() )
		{
			$data	= $request->getPost();
			
			$user = $this->getTable('Admin\Model\User')->getRowByKey( 'email', $data['email'] );

			if( !$user )
			{
				$this->flashMessenger()->addInfoMessage('Desculpe, não encontramos um usuário com este e-mail');

				return $this->redirect()->toRoute( 'forgot' );
			}
			
			$password = Hash::getRandomPassword();

			$user->password = Hash::getPasswordHash( $password );

			$saved =  $this->getTable('Admin\Model\User')->save( $user );

			try
			{						
				$this->sendPasswordMail( $user, $password );

				$this->flashMessenger()->addInfoMessage('Uma nova senha foi enviada para o seu e-mail');
			}
			catch( \Exception $excecao )
			{
				
				var_dump($excecao);
				exit();
				$this->flashMessenger()->addErrorMessage( $excecao->getMessage() );
				$this->flashMessenger()->addErrorMessage( 'Houve um erro e não foi possível reenviar a senha do usuário' );
			}
			
			return $this->redirect()->toRoute( 'login' );
		}
		
		return new ViewModel(array(
            'form' => $form
        ));
	}
	
    /**
     * Mostra o formulário de login
     * @return void
     */
    public function indexAction()
    {
		$auth = new AuthenticationService();
		
		if( $auth->hasIdentity() )
		{
			$this->flashMessenger()->addInfoMessage('Você já está logado no sistema');
			
			return $this->redirect()->toRoute('admin');
		}
		
		$this->layout('layout/login');
		
        $form = new Login();
		
        return new ViewModel(array(
            'form' => $form
        ));
    }
 
    /**
     * Faz o login do usuário
     * @return void
     */
    public function loginAction()
    {
        $request = $this->getRequest();
		
        if( !$request->isPost() )
		{
			$this->flashMessenger()->addErrorMessage('Wrong access');
			
			return $this->redirect()->toRoute('login');
        }
 
        $data		= $request->getPost();
        $service	= $this->getService('Admin\Service\Auth');
		
		try
		{
			$auth = $service->authenticate(
				array(
					'username'	=> $data['username'], 
					'password'	=> $data['password'],
				)
			);

			$this->flashMessenger()->addInfoMessage('Your session was started');

			if( $service->authorize( null, 'Admin\Controller\Index', 'index' ) )
				return $this->redirect()->toRoute('admin');
			
			return $this->redirect()->toRoute('home');
		}
		catch( \Exception $excecao )
		{
			$this->flashMessenger()->addErrorMessage( $excecao->getMessage() );
			
			return $this->redirect()->toRoute('login');
		}
    }
 
    /**
     * Faz o logout do usuário
     * @return void
     */
    public function logoutAction()
    {
        $service	= $this->getService('Admin\Service\Auth');
        $auth		= $service->logout();
        
		$this->flashMessenger()->addMessage('Você foi desconectado');
		
        return $this->redirect()->toRoute('login');
    }
	
	protected function sendPasswordMail( $user, $password )
	{
		$translator = $this->getService('translator');
		
		$generator = $this->getEvent()->getRouteMatch()->getParam('controller', 'index') . '.' . $this->getEvent()->getRouteMatch()->getParam('action', 'index');

		$maildata['priority']	= 0; 
		$maildata['generator']	= $generator;
		$maildata['to_email']	= $user->email;
		$maildata['subject']	= 'CMS - ' . $translator->translate('Sua nova senha');
		$maildata['format']		= 'html';

		$this->renderer = $this->getServiceLocator()->get('ViewRenderer');  
		
		$body = $this->renderer->render(
			'mail/user_password', 
			array(
					'user' => $user,
					'password' => $password,
					'emailSupport' => 'tiago@catafesta.com.br',
					'siteUrl' => 'http://www.catafesta.com',
					'siteName' => 'CMS',
			)
		); 
		
		$maildata['body'] = $body;

		$this->genMail( $maildata );
	}
}