<?php
namespace Admin\Controller;
 
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Core\Controller\ActionController;
use Admin\Form\User as UserForm;

use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelect;

use Zend\Db\Sql\Select;
use Zend\Db\Adapter\Exception\InvalidQueryException;

use Core\Helper\Hash;

use Zend\Db\Sql\Predicate;

class UserController extends ActionController
{	
	public function removeAction()
    {
        $registros = $this->params()->fromPost('registros', array());

		$back = $this->getRequest()->getHeader('Referer', '/admin')->getUri();

        if( !count( $registros ) )
		{
			$this->flashMessenger()->addInfoMessage('Nenhum id enviado');

			return $this->redirect()->toUrl( $back );
		}
        
		try
		{
			foreach( $registros as $id )
			{
				$this->getTable('Admin\Model\User')->delete($id);
			}
			
			$this->flashMessenger()->addInfoMessage('Usuário(s) excluído(s) com sucesso');
		}
		catch( InvalidQueryException $excecao )
		{
			$this->flashMessenger()->addMessage('O(s) associado(s) selecionado(s) não pode ser excluido');
		}
		catch( \Exception $excecao )
		{
			$this->flashMessenger()->addErrorMessage( $excecao->getMessage() );
		}

        return $this->redirect()->toUrl( $back );
    }
	
	public function resendAction()
	{
		$id = (int) $this->params()->fromRoute('id', 0);
		
		$user = $this->getTable('Admin\Model\User')->get( $id );
		
		$back = $this->getRequest()->getHeader('Referer', '/admin')->getUri();
		
		if( !$user )
		{
			$this->flashMessenger()->addInfoMessage('Nenhum usuário encontrado');
			
			return $this->redirect()->toUrl( $back );
		}
		
		$password = Hash::getRandomPassword();
		
		$user->password = Hash::getPasswordHash( $password );
		
		$saved =  $this->getTable('Admin\Model\User')->save( $user );
		
		try
		{						
			$this->sendPasswordMail( $user, $password );
			
			$this->flashMessenger()->addInfoMessage('Senha reenviada');
		}
		catch( \Exception $excecao )
		{			
			var_dump($excecao);
			exit();
			
			$this->flashMessenger()->addErrorMessage( 'Houve um erro ao reenviar a senha por e-mail: ' . $excecao->getMessage() );
		}		

		return $this->redirect()->toUrl( $back );
	}
	
	public function saveAction()
    {
        $form		= new UserForm( $this->getService('DbAdapter') );

		$back = $this->url()->fromRoute( 'user' );
		$form->get('reset')->setAttribute( 'onClick', "window.location='{$back}'" );
		
        $request	= $this->getRequest();
		
        if( $request->isPost() )
		{
			$post = $request->getPost();

            $user = $this->getService('Db\Admin\Model\User');
			
			if( $post->id )
				$user->setRecordId( $post->id );
			
            $form->setInputFilter( $user->getInputFilter() );
            $form->setData( $request->getPost() );

            if( $form->isValid() )
			{
                $data = $form->getData();

				$password = $data['password'];
				
				$user->setData( $data );

				$saved =  $this->getTable('Admin\Model\User')->save( $user );

				if( !$post->id )
				{
					try
					{
						$this->sendWelcomeMail( $user, $password );
					}
					catch( \Exception $excecao )
					{
						$this->flashMessenger()->addErrorMessage('Houve um problema ao enviar o e-mail: ' . $excecao->getMessage());
						
						return $this->redirect()->toRoute('user');
					}
				}

				$this->flashMessenger()->addInfoMessage('Usuário salvo com sucesso');

				return $this->redirect()->toRoute('user');
            }
        }

		$id = (int) $this->params()->fromRoute('id', 0);
		
        if ($id > 0) 
		{
            $user = $this->getTable('Admin\Model\User')->get($id);
            $form->bind($user);
            $form->get('submit')->setAttribute('value', 'Editar');
        }

        return new ViewModel(
            array(
				'form' => $form
			)
        );
    }

	public function indexAction()
	{
		$valid = $this->params()->fromQuery( 'valid', null );
		
		$sql = $this->getTable('Admin\Model\User')->getSql();
		
		$usuarios = $sql->select();
		$usuarios
			->columns( array( 'id', 'name', 'email', 'username', 'valid' ) )
			->order('usuarios.name ASC')
			;
				
		switch( $valid )
		{
			case 'yes':
				$usuarios->where( array( 'valid' => true ) );
				break;
			case 'no':
				$usuarios->where( array( 'valid' => false ) );
				break;
		}
		
		$paginatorAdapter = new PaginatorDbSelect( $usuarios, $sql );
		$paginator = new Paginator( $paginatorAdapter );
		$paginator->setItemCountPerPage( 10 );
		$paginator->setCurrentPageNumber( $this->params()->fromRoute('page') );
		
		return array(
			'registros' => $paginator,
		);
	}
	
	public function sendPasswordMail( $user, $password )
	{
		$translator = $this->getService('translator');
		
		$generator = $this->getEvent()->getRouteMatch()->getParam('controller', 'index') . '.' . $this->getEvent()->getRouteMatch()->getParam('action', 'index');

		$maildata['priority']	= 0; 
		$maildata['generator']	= $generator;
		$maildata['to_email']	= $user->email;
		$maildata['subject']	= 'CMS - ' . $translator->translate('Sua nova senha');
		$maildata['format']		= 'html';

		$this->renderer = $this->getServiceLocator()->get('ViewRenderer');  
		
		$body = $this->renderer->render(
			'mail/user_password', 
			array(
					'user' => $user,
					'password' => $password,
					'emailSupport' => 'tiago@catafesta.com.br',
					'siteUrl' => 'http://www.catafesta.com',
					'siteName' => 'Catafesta Música para Eventos',
			)
		); 
		
		$maildata['body'] = $body;

		$sent = $this->genMail($maildata);
	}
	
	public function sendWelcomeMail( $user, $password )
	{
		$translator = $this->getService('translator');
		
		$generator = $this->getEvent()->getRouteMatch()->getParam('controller', 'index') . '.' . $this->getEvent()->getRouteMatch()->getParam('action', 'index');

		$maildata['priority'] = 0; 
		$maildata['generator'] = $generator;
		$maildata['to_email'] = $user->email;
		$maildata['subject'] = 'CMS - ' . $translator->translate('Bem vindo ao sistema');
		$maildata['format'] = 'html';

		$this->renderer = $this->getServiceLocator()->get('ViewRenderer');  
		
		$body = $this->renderer->render(
			'mail/user_register', 
			array(
					'user' => $user,
					'password' => $password,
					'emailSupport' => 'tiago@catafesta.com.br',
					'siteUrl' => 'http://www.catafesta.com',
					'siteName' => 'Catafesta Música para Eventos',
			)
		); 
		
		$maildata['body'] = $body;

		$sent = $this->genMail($maildata);
	}
}