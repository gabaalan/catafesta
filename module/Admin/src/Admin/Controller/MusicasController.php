<?php
namespace Admin\Controller;
 
use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Admin\Form\Musica as Form;
use Admin\Model\Musica as Model;

use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelect;

use Zend\Db\Adapter\Exception\InvalidQueryException;

use Zend\Stdlib\ErrorHandler;

class MusicasController extends ActionController
{		
	public function removeAction()
    {
        $registros = $this->params()->fromPost('registros', array());

		$back = $this->getRequest()->getHeader('Referer', '/')->getUri();

        if( !count( $registros ) )
		{
			$this->flashMessenger()->addInfoMessage('Nenhum id enviado');

			return $this->redirect()->toUrl( $back );
		}
        
		try
		{
			foreach( $registros as $id )
			{
				$registro = $this->getTable('Admin\Model\Musica')->get($id);
				
				if( $registro )
				{
					$dirname	= realpath( PUBLIC_PATH . '/uploads/musicas' );
					
					ErrorHandler::start();
					unlink( $dirname . '/' . $registro->caminho );
					ErrorHandler::stop();
					
					$this->getTable('Admin\Model\Musica')->delete($id);
				}
			}
			
			$this->flashMessenger()->addInfoMessage('Música(s) excluída(s) com sucesso');
		}
		catch( InvalidQueryException $excecao )
		{
			$this->flashMessenger()->addMessage('A(s) música(s) selecionada(s) não pode ser excluido');
		}
		catch( \Exception $excecao )
		{
			$this->flashMessenger()->addErrorMessage( $excecao->getMessage() );
		}

        return $this->redirect()->toUrl( $back );
    }
	
	public function saveAction()
    {
        $form		= new Form( $this->getService('DbAdapter') );

		$back = $this->url()->fromRoute( 'musicas' );
		$form->get('reset')->setAttribute( 'onClick', "window.location='{$back}'" );
		
        $request	= $this->getRequest();
		
        if( $request->isPost() )
		{
			$post = $request->getPost();

            $register = $this->getService('Db\Admin\Model\Musica');

			if( $post->id )
			{
				$register->setRecordId( $post->id );
				
				$musica = $this->getTable('Admin\Model\Musica')->get( $post->id );
			}
			
			$filter = $register->getInputFilter();
			
			$files = $request->getFiles();

			if( $post->id && empty( $files->caminho['name'] ) )
				$filter->remove('caminho');
			
            $form->setInputFilter( $filter );
            $form->setData( array_merge_recursive( $post->toArray(), $files->toArray() ) );

            if( $form->isValid() )
			{
                $data = $form->getData();
				
				if( !empty( $data['caminho']['name'] ) )
				{
					$cutoff		= preg_replace( '/[^A-z0-9\-\_\.]/', '-', $data['caminho']['name'] );

					$pathinfo	= pathinfo( $cutoff );

					$newname	= $pathinfo['filename'] . uniqid( '_' ) . '.' . $pathinfo['extension'];

					$dirname	= realpath( PUBLIC_PATH . '/uploads/musicas' );

					$path		= $dirname . '/' . $newname;

					ErrorHandler::start();
					move_uploaded_file( $data['caminho']['tmp_name'], $path );
					ErrorHandler::stop(true);
					
					// ----------------------------------------------------------------------------------------

					$data['caminho'] = $newname;					

					// apaga foto antiga
					if( isset( $musica ) && $musica->caminho != $newname )
					{
						ErrorHandler::start();
						unlink( $dirname . '/' . $musica->caminho );
						ErrorHandler::stop();
					}
				}
				else
				{
					unset( $data['caminho'] );
				}

				$register->setData( $data );

				$this->getTable('Admin\Model\Musica')->save( $register );
				
				$this->flashMessenger()->addInfoMessage('Música salva com sucesso');

				return $this->redirect()->toRoute('musicas');
            }
        }

		$id = (int) $this->params()->fromRoute('id', 0);
		
        if ($id > 0) 
		{
            $register = $this->getTable('Admin\Model\Musica')->get($id);
            $form->bind($register);
            $form->get('submit')->setAttribute('value', 'Editar');
        }

        return new ViewModel(
            array(
				'form' => $form
			)
        );
    }

	public function indexAction()
	{
		$sql = $this->getTable('Admin\Model\Musica')->getSql();
		
		$registros = $sql->select();
		$registros
			->columns( array( 'id', 'nome', 'autor', 'tipo', 'caminho' ) )
			->join(
				array( 'c' => 'categorias' ),
				'c.id = musicas.categoria',
				array('categoria' => 'nome'),
				$registros::JOIN_LEFT
			)
			->order('id DESC')
			;
		
		$paginatorAdapter = new PaginatorDbSelect( $registros, $sql );
		$paginator = new Paginator( $paginatorAdapter );
		$paginator->setItemCountPerPage( 40 );
		$paginator->setCurrentPageNumber( $this->params()->fromRoute('page') );
		
		return array(
			'registros' => $paginator,
		);
	}
}