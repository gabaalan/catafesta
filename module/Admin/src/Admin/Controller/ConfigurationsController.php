<?php
namespace Admin\Controller;
 
use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;

use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelect;

use Admin\Form\Configuration as Form;

use Zend\Form\Element;

class ConfigurationsController extends ActionController
{	
	public function saveAction()
    {
        $form		= new Form();

		$id = (int) $this->params()->fromRoute('id', 0) ?: (int) $this->params()->fromPost('id', 0);

		$categoria = $this->getTable('Admin\Model\Configuration')->get($id);
		
		if( !$categoria )
		{
			$this->flashMessenger()->addErrorMessage( 'Não foi possível editar a configuração escolhida' );
				
			return $this->redirect()->toRoute('configuration');
		}
		
		$editors = array(
			'rodape_endereco',
			'pagina_quem_somos',
			'pagina_atuacao',
		);
		
		if( in_array( $categoria->key, $editors ) )
		{		
			$form->remove('value');
			
			$value = new Element\Textarea('value');
			$value
				->setLabel('Modificar valor')
				->setLabelAttributes( array( 'class' => 'col-sm-2 control-label' ) )
				->setAttributes(array(
					'class'	=> 'value-ck form-control',
					'required'	=> 'true',
				));
			
			$form->add($value);
		}
		
		$form->bind($categoria);
		
        $request	= $this->getRequest();
		
        if( $request->isPost() )
		{
			$post = $request->getPost();
			
			$filter = $categoria->getInputFilter();
			
            $form->setInputFilter( $filter );
            $form->setData( $request->getPost() );

            if( $form->isValid() )
			{
                try
				{
					$data	= $form->getData();

					$categoria->setData( $data );

					$saved =  $this->getTable('Admin\Model\Configuration')->save( $categoria );
					
					$this->flashMessenger()->addInfoMessage('Configuração salva com sucesso');
				}
				catch( \Exception $excecao )
				{
					$this->flashMessenger()->addErrorMessage( $excecao->getMessage() );
				}
				
				return $this->redirect()->toRoute('configuration');
            }
        }

        return new ViewModel(
            array(
				'form' => $form
			)
        );
    }

	public function indexAction()
	{
		$sql = $this->getTable('Admin\Model\Configuration')->getSql();
		
		$registros = $sql->select();
		$registros
			->order('group ASC, name ASC')
			;
		
		$paginatorAdapter = new PaginatorDbSelect( $registros, $sql );
		$paginator = new Paginator( $paginatorAdapter );
		$paginator->setItemCountPerPage( 50 ); //Default 10
		$paginator->setCurrentPageNumber( $this->params()->fromRoute('page') );

		return array(
			'registros' => $paginator,
		);
	}
}