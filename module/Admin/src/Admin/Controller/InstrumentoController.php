<?php
namespace Admin\Controller;
 
use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Admin\Form\Musico as Form;
use Admin\Model\Musico as Model;

use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelect;

use Zend\Db\Adapter\Exception\InvalidQueryException;

class InstrumentoController extends ActionController
{	
	public function removeAction()
    {
        $registros = $this->params()->fromPost('registros', array());

		$back = $this->getRequest()->getHeader('Referer', '/')->getUri();

        if( !count( $registros ) )
		{
			$this->flashMessenger()->addInfoMessage('Nenhum id enviado');

			return $this->redirect()->toUrl( $back );
		}
        
		try
		{
			foreach( $registros as $id )
			{
				$this->getTable('Admin\Model\Categoria')->delete($id);
			}
			
			$this->flashMessenger()->addInfoMessage('A(s) categoria(s) foram excluída(s) com sucesso');
		}
		catch( InvalidQueryException $excecao )
		{
			$this->flashMessenger()->addMessage('A(s) categoria(s) selecionada(s) não pode ser excluida (algum registro está ligado à ela)');
		}
		catch( \Exception $excecao )
		{
			$this->flashMessenger()->addErrorMessage( $excecao->getMessage() );
		}

        return $this->redirect()->toUrl( $back );
    }
	
	public function saveAction()
    {
        $form		= new Form();

		$back = $this->url()->fromRoute( 'instrumento' );
		$form->get('reset')->setAttribute( 'onClick', "window.location='{$back}'" );
		
        $request	= $this->getRequest();
		
        if( $request->isPost() )
		{
			$post = $request->getPost();

            $registro = new Model();
			
			if( $post->id )
				$registro->setRecordId( $post->id );
			
            $form->setInputFilter( $registro->getInputFilter() );
            $form->setData( $request->getPost() );

            if( $form->isValid() )
			{
                $data = $form->getData();
				
				$registro->setData( $data );

				$this->getTable('Admin\Model\Musico')->save( $registro );
				
				$this->flashMessenger()->addInfoMessage('Instrumento salvo com sucesso');

				return $this->redirect()->toRoute('instrumento');
            }
        }

		$id = (int) $this->params()->fromRoute('id', 0);
		
        if ($id > 0) 
		{
            $registro = $this->getTable('Admin\Model\Musico')->get($id);
            $form->bind($registro);
            $form->get('submit')->setAttribute('value', 'Editar');
        }

        return new ViewModel(
            array(
				'form' => $form
			)
        );
    }

	public function indexAction()
	{
		$sql = $this->getTable('Admin\Model\Musico')->getSql();
		
		$select = $sql->select();
		$select
			->columns( array( 'id', 'nome' ) )
			->order('nome ASC')
			;
		
		$paginatorAdapter = new PaginatorDbSelect( $select, $sql );
		$paginator = new Paginator( $paginatorAdapter );
		$paginator->setItemCountPerPage( 20 );
		$paginator->setCurrentPageNumber( $this->params()->fromRoute('page') );
		
		return array(
			'registros' => $paginator,
		);
	}
}