<?php
namespace Admin\Controller;
 
use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Admin\Form\User as UserForm;

class ProfileController extends ActionController
{
	public function indexAction()
    {		
        $form		= new UserForm( $this->getService('DbAdapter'), array( 'angel_data_url' => $this->url()->fromRoute( 'user', array( 'action' => 'search' ) ) ) );

		$form->remove('username');
		$form->remove('auth_role_id');
		$form->remove('valid');
		
		$back = $this->url()->fromRoute( 'admin' );
		$form->get('reset')->setAttribute( 'onClick', "window.location='{$back}'" );
		
        $request	= $this->getRequest();
		
        if( $request->isPost() )
		{
			$post = $request->getPost();

            $user = $this->getService('Db\Admin\Model\User');
			
			if( $post->id )
				$user->setRecordId( $post->id );
			
			$filter = $user->getInputFilter();
			
			$filter->remove('username');
			$filter->remove('auth_role_id');
			$filter->remove('valid');
			
            $form->setInputFilter( $filter );
            $form->setData( $request->getPost() );

            if( $form->isValid() )
			{
                $data = $form->getData();
				
				$user->setData( $data );

				$this->getTable('Admin\Model\User')->save( $user );
				
				$this->flashMessenger()->addInfoMessage('Perfil modificado com sucesso');

				return $this->redirect()->toRoute('admin');
            }
        }
		else
		{
			$session	= $this->getService('Session');
			
			$authService = $this->getService('Admin\Service\Auth');
		
            $user = $this->getTable('Admin\Model\User')->get($session->offsetGet( $authService->getSessionName( $authService::NAMESPACE_ADMIN ) )->id);
            $form->bind($user);
            $form->get('submit')->setAttribute('value', 'Modificar');
        }

        return new ViewModel(
            array(
				'form' => $form
			)
        );
    }
}