<?php
namespace Admin\Controller;
 
use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Admin\Form\Depoimento as Form;
use Admin\Model\Depoimento as Model;

use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelect;

use Zend\Db\Adapter\Exception\InvalidQueryException;

use Zend\Stdlib\ErrorHandler;

use PHPImageWorkshop\ImageWorkshop;

class DepoimentoController extends ActionController
{	
	public function statusAction()
    {
        $id		= $this->params()->fromRoute('id', 0);
        $value	= $this->params()->fromRoute('value', 1);

		$back = $this->getRequest()->getHeader('Referer', '/')->getUri();

        if( !$id )
		{
			$this->flashMessenger()->addInfoMessage('Nenhum id enviado');

			return $this->redirect()->toUrl( $back );
		}
        
		try
		{

			$registro = $this->getTable('Admin\Model\Depoimento')->get($id);
			
			switch( $value ) 
			{
				case 0:
					$registro->aprovado = 0;
					$mensagem = 'Depoimento reprovado!';
					break;

				default:
					$registro->aprovado = 1;
					$mensagem = 'Depoimento aprovado!';
					break;
			}
			
			$this->getTable('Admin\Model\Depoimento')->save( $registro );
			
			$this->flashMessenger()->addInfoMessage( $mensagem );
		}
		catch( InvalidQueryException $excecao )
		{
			$this->flashMessenger()->addMessage('Houve um problema ao modificar o status do depoimento');
		}
		catch( \Exception $excecao )
		{
			$this->flashMessenger()->addErrorMessage( $excecao->getMessage() );
		}

        return $this->redirect()->toUrl( $back );
    }
	
	public function removeAction()
    {
        $registros = $this->params()->fromPost('registros', array());

		$back = $this->getRequest()->getHeader('Referer', '/')->getUri();

        if( !count( $registros ) )
		{
			$this->flashMessenger()->addInfoMessage('Nenhum id enviado');

			return $this->redirect()->toUrl( $back );
		}
        
		try
		{
			foreach( $registros as $id )
			{
				$depoimento = $this->getTable('Admin\Model\Depoimento')->get($id);
				
				if( $depoimento )
				{
					$dirname	= realpath( PUBLIC_PATH . '/uploads/depoimentos' );
					
					ErrorHandler::start();
					unlink( $dirname . '/' . $depoimento->foto );
					unlink( $dirname . '/thumb_' . $depoimento->foto );
					ErrorHandler::stop();
					
					$this->getTable('Admin\Model\Depoimento')->delete($id);
				}
			}
			
			$this->flashMessenger()->addInfoMessage('Depoimento(s) excluído(s) com sucesso');
		}
		catch( InvalidQueryException $excecao )
		{
			$this->flashMessenger()->addMessage('O(s) depoimento(s) selecionado(s) não pode ser excluido');
		}
		catch( \Exception $excecao )
		{
			$this->flashMessenger()->addErrorMessage( $excecao->getMessage() );
		}

        return $this->redirect()->toUrl( $back );
    }
	
	public function saveAction()
    {
        $form		= new Form(); //$this->getService('DbAdapter')

		$back = $this->url()->fromRoute( 'depoimento' );
		$form->get('reset')->setAttribute( 'onClick', "window.location='{$back}'" );
		
        $request	= $this->getRequest();
		
        if( $request->isPost() )
		{
			$post = $request->getPost();

            $register = new Model();
			
			if( $post->id )
			{
				$register->setRecordId( $post->id );
				
				$depoimento = $this->getTable('Admin\Model\Depoimento')->get( $post->id );
			}
			
			$filter = $register->getInputFilter();
			
			$files = $request->getFiles();
			
			if( $post->id && empty( $files->content['name'] ) )
				$filter->remove('foto');
			
            $form->setInputFilter( $filter );
            $form->setData( array_merge_recursive( $post->toArray(), $files->toArray() ) );

            if( $form->isValid() )
			{
                $data = $form->getData();

				if( !empty( $data['foto']['name'] ) )
				{
					$cutoff		= preg_replace( '/[^A-z0-9\-\_\.]/', '-', $data['foto']['name'] );
					
					$pathinfo	= pathinfo( $cutoff );

					$newname	= $pathinfo['filename'] . uniqid( '_' ) . '.' . $pathinfo['extension'];

					$dirname	= realpath( PUBLIC_PATH . '/uploads/depoimentos' );

					$path		= $dirname . '/' . $newname;
					
					ErrorHandler::start();
					move_uploaded_file( $data['foto']['tmp_name'], $path );
					ErrorHandler::stop(true);

					// ----------------------------------------------------------------------------------------
					// Gera os thumbs

					$imagem = ImageWorkshop::initFromPath( $path );

					$width	= 97;
					$height	= 97;

					$imagem->cropMaximumInPixel(0, 0, "MM");
					$imagem->resizeInPixel( $width, $height );

					$imagem->save( $dirname, 'thumb_' . $newname, true, null, 100 );

					// ----------------------------------------------------------------------------------------

					$data['foto'] = $newname;					

					// apaga foto antiga
					if( isset( $depoimento ) && $depoimento->foto != $newname )
					{
						ErrorHandler::start();
						unlink( $dirname . '/' . $depoimento->foto );
						unlink( $dirname . '/thumb_' . $depoimento->foto );
						ErrorHandler::stop();
					}
				}
				else
				{
					unset( $data['foto'] );
				}

				$register->setData( $data );

				$this->getTable('Admin\Model\Depoimento')->save( $register );
				
				$this->flashMessenger()->addInfoMessage('Depoimento salvo com sucesso');

				return $this->redirect()->toRoute('depoimento');
            }
        }

		$id = (int) $this->params()->fromRoute('id', 0);
		
        if ($id > 0) 
		{
            $register = $this->getTable('Admin\Model\Depoimento')->get($id);
            $form->bind($register);
            $form->get('submit')->setAttribute('value', 'Editar');
        }

        return new ViewModel(
            array(
				'form' => $form
			)
        );
    }

	public function indexAction()
	{
		$sql = $this->getTable('Admin\Model\Depoimento')->getSql();
		
		$registros = $sql->select();
		$registros
			->columns( array( 'id', 'nome', 'email', 'mensagem', 'foto', 'aprovado' ) )
			->order('id DESC')
			;
		
		$paginatorAdapter = new PaginatorDbSelect( $registros, $sql );
		$paginator = new Paginator( $paginatorAdapter );
		$paginator->setItemCountPerPage( 10 );
		$paginator->setCurrentPageNumber( $this->params()->fromRoute('page') );
		
		return array(
			'registros' => $paginator,
		);
	}
}