<?php
namespace Admin\Controller;
 
use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Admin\Form\Categoria as Form;
use Admin\Model\Categoria as Model;

use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelect;

use Zend\Db\Adapter\Exception\InvalidQueryException;

class CategoriaController extends ActionController
{	
	public function removeAction()
    {
        $registros = $this->params()->fromPost('registros', array());

		$back = $this->getRequest()->getHeader('Referer', '/')->getUri();

        if( !count( $registros ) )
		{
			$this->flashMessenger()->addInfoMessage('Nenhum id enviado');

			return $this->redirect()->toUrl( $back );
		}
        
		try
		{
			foreach( $registros as $id )
			{
				$this->getTable('Admin\Model\Categoria')->delete($id);
			}
			
			$this->flashMessenger()->addInfoMessage('A(s) categoria(s) foram excluída(s) com sucesso');
		}
		catch( InvalidQueryException $excecao )
		{
			$this->flashMessenger()->addMessage('A(s) categoria(s) selecionada(s) não pode ser excluida (algum registro está ligado à ela)');
		}
		catch( \Exception $excecao )
		{
			$this->flashMessenger()->addErrorMessage( $excecao->getMessage() );
		}

        return $this->redirect()->toUrl( $back );
    }
	
	public function saveAction()
    {
        $form		= new Form( $this->getService('DbAdapter') );

		$back = $this->url()->fromRoute( 'categoria' );
		$form->get('reset')->setAttribute( 'onClick', "window.location='{$back}'" );
		
        $request	= $this->getRequest();
		
        if( $request->isPost() )
		{
			$post = $request->getPost();

            $registro = $this->getService('Db\Admin\Model\Categoria');
			
			if( $post->id )
				$registro->setRecordId( $post->id );
			
            $form->setInputFilter( $registro->getInputFilter() );
            $form->setData( $request->getPost() );

            if( $form->isValid() )
			{
                $data = $form->getData();
				
				$registro->setData( $data );

				$this->getTable('Admin\Model\Categoria')->save( $registro );
				
				$this->flashMessenger()->addInfoMessage('Categoria salva com sucesso');

				return $this->redirect()->toRoute('categoria');
            }
        }

		$id = (int) $this->params()->fromRoute('id', 0);
		
        if ($id > 0) 
		{
            $registro = $this->getTable('Admin\Model\Categoria')->get($id);
            $form->bind($registro);
            $form->get('submit')->setAttribute('value', 'Editar');
        }

        return new ViewModel(
            array(
				'form' => $form
			)
        );
    }

	public function indexAction()
	{
		$sql = $this->getTable('Admin\Model\Categoria')->getSql();
		
		$select = $sql->select();
		$select
			->columns( array( 'id', 'nome' ) )
			->join(
				array( 'ct' => 'categoria_tipos' ),
				'ct.id = categorias.tipo_id',
				array('tipo' => 'nome'),
				$select::JOIN_INNER
			)
			->order('categorias.nome ASC')
			;
		
		$paginatorAdapter = new PaginatorDbSelect( $select, $sql );
		$paginator = new Paginator( $paginatorAdapter );
		$paginator->setItemCountPerPage( 10 );
		$paginator->setCurrentPageNumber( $this->params()->fromRoute('page') );
		
		return array(
			'registros' => $paginator,
		);
	}
}