<?php
namespace Admin\Controller;
 
use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
 
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Predicate\Predicate;

/**
 * Pagina inicial
 * 
 * @category Admin
 * @package Controller
 * @author  Gaba
 */
class IndexController extends ActionController
{
	public function indexAction()
	{
		$sql = $this->getTable('Admin\Model\Depoimento')->getSql();
		
		$select = $sql->select();
		$select
			->columns( array( 'id', 'nome', 'email', 'mensagem', 'foto' ) )
			->where( 'aprovado IS NULL' )
			->order('depoimentos.id ASC')
			;
		
		$statement = $sql->prepareStatementForSqlObject( $select );
		
		$depoimentos = $statement->execute();
		
		return new ViewModel(
			array(
				'depoimentos'	=> $depoimentos,
			)
		);
	}
}