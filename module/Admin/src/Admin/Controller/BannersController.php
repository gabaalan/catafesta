<?php
namespace Admin\Controller;
 
use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;

use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelect;

use Zend\Stdlib\ErrorHandler;

use PHPImageWorkshop\ImageWorkshop;

use Admin\Form\Banner as Form;

class BannersController extends ActionController
{	
	public function removeAction()
    {
        $registros = $this->params()->fromPost('registros', array());

		$back = $this->getRequest()->getHeader('Referer', '/admin')->getUri();

        if( !count( $registros ) )
		{
			$this->flashMessenger()->addInfoMessage('Nenhum id enviado');

			return $this->redirect()->toUrl( $back );
		}
        
		try
		{
			foreach( $registros as $id )
			{
				$banner = $this->getTable('Admin\Model\Banner')->get($id);
				
				if( $banner )
				{
					$dirname	= realpath( PUBLIC_PATH . '/uploads/banners' );
					
					ErrorHandler::start();
					unlink( $dirname . '/' . $banner->path );
					unlink( $dirname . '/banner_' . $banner->path );
					ErrorHandler::stop();
					
					$this->getTable('Admin\Model\Banner')->delete($id);
				}
			}
			
			$this->flashMessenger()->addInfoMessage('Banner excluído com sucesso');
		}
		catch( InvalidQueryException $excecao )
		{
			$this->flashMessenger()->addMessage('O banner selecionado não pôde ser excluído');
		}
		catch( \Exception $excecao )
		{
			$this->flashMessenger()->addErrorMessage( $excecao->getMessage() );
		}

        return $this->redirect()->toUrl( $back );
    }
	
	public function saveAction()
    {
        $form		= new Form();

        $request	= $this->getRequest();
		
        if( $request->isPost() )
		{
			$post = $request->getPost();

            $banner = new \Admin\Model\Banner;

			if( $post->id )
				$banner->setRecordId( $post->id );
			
			$filter = $banner->getInputFilter();
			
			$files = $request->getFiles();
			
			if( $post->id && empty( $files->path['name'] ) )
				$filter->remove('path');
			
            $form->setInputFilter( $filter );
            $form->setData( array_merge_recursive( $request->getPost()->toArray(), $request->getFiles()->toArray() ) );

            if( $form->isValid() )
			{
                try
				{
					$data	= $form->getData();

					if( !empty( $data['path']['name'] ) )
					{
                        set_time_limit( 320 );
                        ini_set('memory_limit', '-1');

						$cutoff		= preg_replace( '/[^A-z0-9\-\_\.]/', '-', $data['path']['name'] );

						$pathinfo	= pathinfo( $cutoff );

						$newname	= $pathinfo['filename'] . uniqid( '_' ) . '.' . $pathinfo['extension'];

						$dirname	= realpath( PUBLIC_PATH . '/uploads/banners' );

						$path		= $dirname . '/' . $newname;

						/*ErrorHandler::start();
						move_uploaded_file( $data['path']['tmp_name'], $path );
						ErrorHandler::stop(true);*/

						// ----------------------------------------------------------------------------------------
						// Gera os thumbs

                        $imagem = ImageWorkshop::initFromPath( $data['path']['tmp_name'] ); //$path );

                        $imagem->save( $dirname, $newname );

						$width	= 1200;
						$height	= 400;

						if( $imagem->getWidth() > $width )
						{
							$imagem->resizeInPixel( $width, null, true );
						}

                        if( $imagem->getHeight() > $height )
                        {
                            $imagem->cropInPixel( $width, $height, 0, 0, 'TM' );
                        }
							
						$imagem->save( $dirname, 'banner_' . $newname, true, null, 100 );

						// ----------------------------------------------------------------------------------------

						$data['path'] = $newname;
					}
					else
					{
						unset( $data['path'] );
					}
					
					$banner->setData( $data );

					$saved =  $this->getTable('Admin\Model\Banner')->save( $banner );
					
					$this->flashMessenger()->addInfoMessage('Banner salvo com sucesso');
				}
				catch( \Exception $excecao )
				{
					$this->flashMessenger()->addErrorMessage( 'Erro: ' . $excecao->getMessage() );
				}
				
				return $this->redirect()->toRoute('banners');
            }
        }

		$id = (int) $this->params()->fromRoute('id', 0);
		
        if ($id > 0) 
		{
            $banner = $this->getTable('Admin\Model\Banner')->get($id);
            $form->bind($banner);
            $form->get('submit')->setAttribute('value', 'Editar');
        }

        return new ViewModel(
            array(
				'form' => $form
			)
        );
    }

	public function indexAction()
	{
		$sql = $this->getTable('Admin\Model\Banner')->getSql();
		
		$registros = $sql->select();
		$registros
			->order('id DESC')
			;
		
		$paginatorAdapter = new PaginatorDbSelect( $registros, $sql );
		$paginator = new Paginator( $paginatorAdapter );
		$paginator->setItemCountPerPage( 20 );
		$paginator->setCurrentPageNumber( $this->params()->fromRoute('page') );

		return array(
			'registros' => $paginator,
		);
	}
}