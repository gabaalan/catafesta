<?php
namespace Admin\Controller;
 
use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Admin\Form\Profissional as Form;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelect;
use Zend\Db\Adapter\Exception\InvalidQueryException;

class ParceirosController extends ActionController
{		
	public function removeAction()
    {
        $registros = $this->params()->fromPost('registros', array());

		$back = $this->getRequest()->getHeader('Referer', '/')->getUri();

        if( !count( $registros ) )
		{
			$this->flashMessenger()->addInfoMessage('Nenhum id enviado');

			return $this->redirect()->toUrl( $back );
		}
        
		try
		{
			foreach( $registros as $id )
			{
				$this->getTable('Admin\Model\Profissional')->delete($id);
			}
			
			$this->flashMessenger()->addInfoMessage('Parceiro(s) excluído(s) com sucesso');
		}
		catch( InvalidQueryException $excecao )
		{
			$this->flashMessenger()->addMessage('O(s) parceiros(s) selecionada(s) não pode ser excluido');
		}
		catch( \Exception $excecao )
		{
			$this->flashMessenger()->addErrorMessage( $excecao->getMessage() );
		}

        return $this->redirect()->toUrl( $back );
    }
	
	public function saveAction()
    {
        $form		= new Form( $this->getService('DbAdapter') );

		$back = $this->url()->fromRoute( 'parceiro' );
		$form->get('reset')->setAttribute( 'onClick', "window.location='{$back}'" );
		
        $request	= $this->getRequest();
		
        if( $request->isPost() )
		{
			$post = $request->getPost();

            $register = $this->getService('Db\Admin\Model\Profissional');

			if( $post->id )
			{
				$register->setRecordId( $post->id );
			}
			
			$filter = $register->getInputFilter();
			
            $form->setInputFilter( $filter );
            $form->setData( $post->toArray() );
			
            if( $form->isValid() )
			{
                $data = $form->getData();

				$register->setData( $data );

				$this->getTable('Admin\Model\Profissional')->save( $register );
				
				$this->flashMessenger()->addInfoMessage('Parceiro salvo com sucesso');

				return $this->redirect()->toRoute('parceiro');
            }
        }

		$id = (int) $this->params()->fromRoute('id', 0);
		
        if ($id > 0) 
		{
            $register = $this->getTable('Admin\Model\Profissional')->get($id);
            $form->bind($register);
            $form->get('submit')->setAttribute('value', 'Editar');
        }

        return new ViewModel(
            array(
				'form' => $form
			)
        );
    }

	public function indexAction()
	{
		$sql = $this->getTable('Admin\Model\Profissional')->getSql();
		
		$registros = $sql->select();
		$registros
			->columns( array( 'id', 'nome', 'telefone', 'email', 'site', 'descricao' ) )
			->join(
				array( 'c' => 'categorias' ),
				'c.id = profissionais.categoria_id',
				array('categoria' => 'nome'),
				$registros::JOIN_LEFT
			)
			->order('id DESC')
			;
		
		$paginatorAdapter = new PaginatorDbSelect( $registros, $sql );
		$paginator = new Paginator( $paginatorAdapter );
		$paginator->setItemCountPerPage( 40 );
		$paginator->setCurrentPageNumber( $this->params()->fromRoute('page') );
		
		return array(
			'registros' => $paginator,
		);
	}
}