<?php
namespace Admin\Controller;
 
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Core\Controller\ActionController;

use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelect;
use Zend\Stdlib\ErrorHandler;

use PHPImageWorkshop\ImageWorkshop;

use Admin\Model\Foto;

class FotosController extends ActionController
{	
	public function removeAction()
    {
        $registros = $this->params()->fromPost('registros', array());

		$back = $this->getRequest()->getHeader('Referer', '/admin')->getUri();

        if( !count( $registros ) )
		{
			$this->flashMessenger()->addInfoMessage('Nenhum id enviado');

			return $this->redirect()->toUrl( $back );
		}
        
		try
		{
			foreach( $registros as $id )
			{
				$image = $this->getTable('Admin\Model\Foto')->get($id);
				
				if( $image )
				{
					$dirname	= realpath( PUBLIC_PATH . '/uploads/fotos' );
					
					ErrorHandler::start();
					unlink( $dirname . '/' . $image->path );
					unlink( $dirname . '/thumb_' . $image->path );
					ErrorHandler::stop();
				}
				
				$this->getTable('Admin\Model\Foto')->delete($id);
			}
			
			$this->flashMessenger()->addInfoMessage('Foto excluída com sucesso');
		}
		catch( InvalidQueryException $excecao )
		{
			$this->flashMessenger()->addMessage('A foto selecionada não pôde ser excluída');
		}
		catch( \Exception $excecao )
		{
			$this->flashMessenger()->addErrorMessage( $excecao->getMessage() );
		}

        return $this->redirect()->toUrl( $back );
    }
	
	public function uploadAction()
	{
		$this->getResponse()->getHeaders()->addHeaderLine( 'Expires', 'Mon, 26 Jul 1997 05:00:00 GMT' );
		$this->getResponse()->getHeaders()->addHeaderLine( 'Last-Modified', gmdate("D, d M Y H:i:s") . " GMT" );
		$this->getResponse()->getHeaders()->addHeaderLine( 'Cache-Control', 'no-store, no-cache, must-revalidate' );
		$this->getResponse()->getHeaders()->addHeaderLine( 'Cache-Control', 'post-check=0, pre-check=0' );
		$this->getResponse()->getHeaders()->addHeaderLine( 'Pragma', 'no-cache' );
		
		set_time_limit( 320 );
		ini_set('memory_limit', '-1');
		
		$data = array_merge_recursive(
			$this->getRequest()->getPost()->toArray(),
			$this->getRequest()->getFiles()->toArray()
		);
		
		try
		{
			if( isset( $data['image'] ) && !empty( $data['image']['name'] ) )
			{
				$imageEntity = new Foto();

				// ----------------------------------------------------------------------------------------
				
				$cutoff		= preg_replace( '/[^A-z0-9\-\_\.]/', '-', $data['image']['name'] );

				$pathinfo	= pathinfo( $cutoff );

				if( !isset( $pathinfo['filename'] ) || !isset( $pathinfo['extension'] ) )
					throw new \Exception('Nenhum arquivo encontrado');
				
				if( $data['image']['error'] > 0 )
				{
					switch( $data['image']['error'] )
					{
						case UPLOAD_ERR_INI_SIZE:
							$mensagem = 'O arquivo no upload é maior do que o limite definido em upload_max_filesize no php.ini';
							break;
						case UPLOAD_ERR_FORM_SIZE:
							$mensagem = 'O arquivo ultrapassa o limite de tamanho em MAX_FILE_SIZE que foi especificado no formulário HTML';
							break;
						case UPLOAD_ERR_PARTIAL:
							$mensagem = 'O upload do arquivo foi feito parcialmente.';
							break;
						case UPLOAD_ERR_NO_FILE:
							$mensagem = 'Não foi feito o upload do arquivo.';
							break;
						default:
							$mensagem = 'Erro ao enviar arquivo';
							break;
					}
					
					throw new \Exception( $mensagem );
				}
				
				$newname	= $pathinfo['filename'] . uniqid( '_' ) . '.' . $pathinfo['extension'];

				$dirname	= realpath( PUBLIC_PATH . '/uploads/fotos' );

				$path		= $dirname . '/' . $newname;
				
//				ErrorHandler::start();
//				move_uploaded_file( $data['image']['tmp_name'], $path );
//				ErrorHandler::stop(true);

				// ----------------------------------------------------------------------------------------
				// Gera os thumbs

				$imagem = ImageWorkshop::initFromPath( $data['image']['tmp_name'] ); //$path );

				$imagem->save( $dirname, $newname );

				$width	= 97;
				$height	= 97;

				$imagem->cropMaximumInPixel(0, 0, "MT" ); // MM = middle
				$imagem->resizeInPixel( $width, $height );
				//$imagem->cropInPixel( $width, $height, 0, 0, 'MM' );

				$imagem->save( $dirname, 'thumb_' . $newname );

				// ----------------------------------------------------------------------------------------

				$imageEntity->setData(
					array(
						'path'	=> $newname,
					)
				);
				
				$this->getTable('Admin\Model\Foto')->save( $imageEntity );
			}
			
			$result = array(
				'jsonrpc'	=> '2.0',
				'result'	=> true,
			);	
		}
		catch( \Exception $excecao )
		{
			$result = array(
				'jsonrpc'	=> '2.0',
				'result'	=> false,
				'message'	=> $excecao->getMessage(), 
				'code'		=> $excecao->getCode(), 
				'file'		=> $excecao->getFile(),
				'line'		=> $excecao->getLine()
			);
		}		
		
		$view = new JsonModel( $result );
		
		return $view;
	}
	
	public function addAction()
    {
        return array();
    }

	public function indexAction()
	{		
		$sql = $this->getTable('Admin\Model\Foto')->getSql();
		
		$registros = $sql->select();
		$registros
			->order('id DESC')
			;
		
		$paginatorAdapter = new PaginatorDbSelect( $registros, $sql );
		$paginator = new Paginator( $paginatorAdapter );
		$paginator->setItemCountPerPage( 40 ); //Default 10
		$paginator->setCurrentPageNumber( $this->params()->fromRoute('page') );

		return array(
			'registros' => $paginator,
		);
	}
}