<?php
namespace Admin\Service;
 
use Core\Service\Service;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\Authentication\Storage\Session;
//use Zend\Db\Sql\Select;

use Core\Helper\Hash;

/**
 * Serviço responsável pela autenticação da aplicação
  * 
 * @category Admin
 * @package Service
 * @author  Gabriel
 */
class Auth extends Service
{
	/**
	 * Adapter usado para a autenticação
	 * @var Zend\Db\Adapter\Adapter
	 */
	private $dbAdapter;

	const NAMESPACE_ADMIN		= 'Admin';
	const NAMESPACE_ASSOCIATE	= 'Assoc';
	
	private $sessionNames	= array(
		self::NAMESPACE_ADMIN		=> 'admin',
		self::NAMESPACE_ASSOCIATE	=> 'associate',
	);
	
	/** 
	 * Construtor da classe
	 *
	 * @return void
	 */
	public function __construct($dbAdapter = null)
	{
		$this->dbAdapter = $dbAdapter;
	}

	/**
	 * Cria um objeto AuthService
	 * @param type $namespace
	 * @return \Zend\Authentication\AuthenticationService
	 */
	public function getAuthenticationService( $namespace = self::NAMESPACE_ADMIN )
	{
		$auth = new AuthenticationService();
		
		$sessionStorage = new Session( $namespace );
		
		$auth->setStorage( $sessionStorage );
		
		return $auth;
	}
	
	public function getSessionName( $namespace )
	{
		return $this->sessionNames[ $namespace ];
	}
	
	/**
	 * Faz a autenticação dos usuários
	 * 
	 * @param array $params
	 * @return array
	 */
	public function authenticate($params)
	{
		if( 
			!isset( $params['username'] ) || 
			!isset( $params['password'] ) ||
			empty( $params['username'] ) ||
			empty( $params['password'] )
		)
		{
			throw new \Exception("Parâmetros inválidos");
		}

		if( !isset( $params['namespace'] ) )
			$params['namespace'] = self::NAMESPACE_ADMIN;
		
		$password = Hash::getPasswordHash( $params['password'] );

		$auth = $this->getAuthenticationService( $params['namespace'] );

		$authAdapter = new AuthAdapter($this->dbAdapter);
		
		$authAdapter
			->setTableName('usuarios')
			->setIdentityColumn('username')
			->setCredentialColumn('password')
			->setIdentity($params['username'])
			->setCredential($password);
		
		$result = $auth->authenticate($authAdapter);

		if( !$result->isValid() )
		{
			throw new \Exception("Login ou senha inválidos");
		}
		
		$user = $authAdapter->getResultRowObject();
		
		if( !$user->valid )
		{
			$auth->clearIdentity();
			
			throw new \Exception("Este usuário está inativo");
			
			return false;
		}
		
		$sessionName = $this->getSessionName( $params['namespace'] );

		//salva o user na sessão
		$session = $this->getServiceManager()->get('Session');        
		$session->offsetSet($sessionName, $authAdapter->getResultRowObject());

		return true;
	}

	/**
	 * Faz a autorização do usuário para acessar o recurso
	 * @param string $moduleName Nome do módulo sendo acessado
	 * @param string $controllerName Nome do controller
	 * @param string $actionName Nome da ação
	 * @return boolean
	 */
	public function authorize( $moduleName, $controllerName, $actionName, $namespace = self::NAMESPACE_ADMIN )
	{
		$auth = $this->getAuthenticationService( $namespace );
		
		if( !$auth->hasIdentity() )
		{
			return $controllerName == 'Admin\Controller\Auth' ? true : false;
		}

		$session	= $this->getServiceManager()->get('Session');
		$user		= $session->offsetGet( $this->getSessionName( $namespace ) );

		$role		= 'Admin';

		$resource	= $controllerName . '.' . $actionName;

		//if( $moduleName != 'admin' )
			//return true;

		$acl = $this->getServiceManager()->get('Core\Acl\Builder')->build();

		if( !$acl->hasResource( $resource ) || $acl->isAllowed( $role, $resource ) )
			return true;

		return false;
	}

	/**
	 * Faz o logout do sistema
	 *
	 * @return void
	 */
	public function logout( $namespace = self::NAMESPACE_ADMIN ) 
	{
		$auth = $this->getAuthenticationService( $namespace );
		
		$session = $this->getServiceManager()->get('Session');
		$session->offsetUnset( $this->getSessionName( $namespace ) );

		$auth->clearIdentity();

		return true;
	} 
}