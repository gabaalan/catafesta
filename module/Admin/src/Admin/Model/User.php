<?php
namespace Admin\Model;
 
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Core\Model\Entity;

use Core\Helper\Hash;
 
/**
 * Entidade User
 * 
 * @category Admin
 * @package Model
 */
class User extends Entity
{
    /**
     * Nome da tabela. Campo obrigatório
     * @var string
     */
    protected $tableName ='usuarios';
 
    /**
     * @var int
     */
    protected $id;
 
    /**
     * @var string
     */
    protected $username;
 
    /**
     * @var string
     */
    protected $password;
 
    /**
     * @var string
     */
    protected $name;
	
    /**
     * @var string
     */
    protected $email;
	
    /**
     * @var int
     */
    protected $valid;
	
	/**
	 * DBAdapter
	 * @var \Zend\Db\Adapter\Adapter 
	 */
	private $dbAdapter;
	
	/**
	 * Minimo de caracteres que uma senha pode ter
	 * @var Int
	 */
	private $passwordMinimum = 6;
	
	/**
	 * Minimo de caracteres que um usuário pode ter
	 * @var Int
	 */
	private $usernameMinimum = 4;
	
	public function setData($data)
	{
		if( isset( $data['password'] ) && !empty( $data['password'] ) )
			$data['password'] = Hash::getPasswordHash( $data['password'] );
		
		parent::setData($data);
	}
	
	public function getData()
	{
		$data = array_filter( 
			get_object_vars( $this ), 
			function( $value ) 
			{ 
				return is_null( $value ) || $value === false || $value === 0 || $value === '' ? false : true;
			} 
		);

		unset( $data['primaryKeyField'] );
		unset( $data['inputFilter'] );
		unset( $data['tableName'] );
		unset( $data['dbAdapter'] );
		unset( $data['passwordMinimum'] );
		unset( $data['usernameMinimum'] );
		
		return $data;
	}
	
	/**
	 * Seta o Adapter do DB
	 * @param \Zend\Db\Adapter\Adapter $dbAdapter
	 */
	public function setDbAdapter( \Zend\Db\Adapter\Adapter $dbAdapter )
	{
		$this->dbAdapter = $dbAdapter;
	}
	
	/**
	 * Getter do Adapter do DB
	 */
	public function getDbAdapter()
	{
		return $this->dbAdapter;
	}
	
	/**
	 * Seta o minimo de caracteres que um pass pode ter
	 * @param Int $length
	 */
	public function setMinimumPasswordLength( $length )
	{
		$this->passwordMinimum = $length;
	}
	
	/**
	 * Seta o minimo de caracteres que um pass pode ter
	 * @param Int $length
	 */
	public function setMinimumUsernameLength( $length )
	{
		$this->usernameMinimum = $length;
	}
	
	/**
	 * Seta o ID do registro
	 * @param Int $id
	 */
	public function setRecordId( $id )
	{
		$this->id = $id;
	}
	
    /**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
	public function getInputFilter()
	{
		if( !$this->inputFilter ) 
		{
			//$config = new \Core\Helper\Config( $this->dbAdapter );
			
			$this->passwordMinimum = (int) 4;
			$this->usernameMinimum = (int) 4; //$config->get('security_min_username_size')
			
			$inputFilter = new InputFilter();
			$factory     = new InputFactory();

			$inputFilter->add($factory->createInput(array(
				'name'     => 'id',
				'required' => true,
				'filters'  => array(
					array('name' => 'Int'),
				),
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name'     => 'valid',
				'required' => true,
			)));

			$inputFilter->add($factory->createInput(array(
				'name'     => 'username',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => $this->usernameMinimum,
							'max'      => 100,
							'message' => 'A quantidade de caracteres mínima para o usuário é ' . $this->usernameMinimum,
						),
					),
					array(
						'name'    => 'Db_NoRecordExists',
						'options' => array(
							'message' => 'Este nome de usuário já existe no sistema',
							'table' => 'usuarios',
							'field' => 'username',
							'adapter' => $this->dbAdapter,
							'exclude' => array(
								'field' => 'id',
								'value' => !is_null( $this->id ) && !empty( $this->id ) ? $this->id : 0
							)
						),
					),
				),
			)));

			$inputFilter->add($factory->createInput(array(
				'name'     => 'password',
				'required' => !is_null( $this->id ) && !empty( $this->id ) ? false : true,
				'filters'  => array(
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => $this->passwordMinimum,
							'max'      => 100,
							'message' => 'A quantidade de caracteres mínima para senha é ' . $this->passwordMinimum,
						),
					),
				),
			)));

			$inputFilter->add($factory->createInput(array(
				'name'     => 'name',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
			)));

			$inputFilter->add($factory->createInput(array(
				'name'     => 'email',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'EmailAddress',
						'options' => array(
							'message' => 'Este e-mail é inválido',
						)
					),
					array(
						'name'    => 'Db_NoRecordExists',
						'options' => array(
							'message' => 'Este e-mail já existe no sistema',
							'table' => 'usuarios',
							'field' => 'email',
							'adapter' => $this->dbAdapter,
							'exclude' => array(
								'field' => 'id',
								'value' => !is_null( $this->id ) && !empty( $this->id ) ? $this->id : 0
							)
						),
					),
				),
			)));
			
			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}
}