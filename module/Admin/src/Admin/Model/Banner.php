<?php
namespace Admin\Model;
 
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Core\Model\Entity;
 
/**
 * Entidade AuthRole
 * 
 * @category Admin
 * @package Model
 */
class Banner extends Entity
{
    /**
     * Nome da tabela. Campo obrigatório
     * @var string
     */
    protected $tableName = 'banners';
 
    /**
     * @var int
     */
    protected $id;
 
    /**
     * @var string
     */
    protected $path;

    /**
     * @var string
     */
    protected $descricao;

    /**
     * @var timestamp
     */
    protected $limite;

	/**
	 * DBAdapter
	 * @var \Zend\Db\Adapter\Adapter 
	 */
	private $dbAdapter;
	
	/**
	 * Seta o Adapter do DB
	 * @param \Zend\Db\Adapter\Adapter $dbAdapter
	 */
	public function setDbAdapter( \Zend\Db\Adapter\Adapter $dbAdapter )
	{
		$this->dbAdapter = $dbAdapter;
	}
	
	/**
	 * Getter do Adapter do DB
	 */
	public function getDbAdapter()
	{
		return $this->dbAdapter;
	}
	
	/**
	 * Seta o ID do registro
	 * @param Int $id
	 */
	public function setRecordId( $id )
	{
		$this->id = $id;
	}

    public function setData($data)
    {
        if( isset( $data['hora'] ) )
            unset( $data['hora'] );

        if( isset( $data['limite'] ) )
        {
            $date = \DateTime::createFromFormat( 'd/m/Y H:i:s', $data['limite'] );

            if( !$date )
                $date = \DateTime::createFromFormat( 'Y-m-d H:i:s', $data['limite'] );

            if( $date )
                $data['limite'] = $date->format( 'Y-m-d H:i:s' );
        }

        parent::setData($data);
    }

	public function getData()
	{
		$data = array_filter( get_object_vars( $this ) );

		unset( $data['primaryKeyField'] );
		unset( $data['inputFilter'] );
		unset( $data['tableName'] );
		unset( $data['dbAdapter'] );
		
		return $data;
	}
	
    /**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
    public function getInputFilter()
    {
        if( !$this->inputFilter )
		{
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
 
            $inputFilter->add($factory->createInput(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));
 
            $inputFilter->add($factory->createInput(array(
				'name'     => 'path',
				'required' => true,
				'filters'  => array(
					//array('name' => 'FileLowercase'), // Não usar pois isto corrompe a imagem
				),
				'validators'  => array(
					array(
						'name' => 'FileSize',
						'options' => array(
							'min'	=> '1kB',
							'max'	=> '6MB',
						),
					),
					array(
						'name' => 'FileImageSize',
						'options' => array(
							'minWidth'	=> 300,
							'maxWidth'	=> 3500,
                            'message' => 'A largura máxima permitida é 3500px'
						),
					),
					array(
						'name' => 'FileMimeType',
						'options' => array(
							'mimeType'	=> array( 'image/gif,image/jpg,image/jpeg,image/png,image/x-png' )
						),
					),
				),
			)));

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name'     => 'limite',
                        'required' => false,
                        'validators' => array(
                            array(
                                'name'    => 'Date',
                                'options' => array(
                                    'format' => 'd/m/Y H:i:s',
                                ),
                            ),
                        ),
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name'     => 'descricao',
                        'required' => true,
                        'filters'  => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )
                )
            );
 
            $this->inputFilter = $inputFilter;
        }
 
        return $this->inputFilter;
    }
}