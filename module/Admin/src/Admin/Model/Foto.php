<?php
namespace Admin\Model;
 
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Core\Model\Entity;
 
/**
 * Entidade AuthRole
 * 
 * @category Admin
 * @package Model
 */
class Foto extends Entity
{
    /**
     * Nome da tabela. Campo obrigatório
     * @var string
     */
    protected $tableName = 'fotos';
 
    /**
     * @var int
     */
    protected $id;
 
    /**
     * @var string
     */
    protected $path;
	
	/**
	 * DBAdapter
	 * @var \Zend\Db\Adapter\Adapter 
	 */
	private $dbAdapter;
	
	/**
	 * Seta o Adapter do DB
	 * @param \Zend\Db\Adapter\Adapter $dbAdapter
	 */
	public function setDbAdapter( \Zend\Db\Adapter\Adapter $dbAdapter )
	{
		$this->dbAdapter = $dbAdapter;
	}
	
	/**
	 * Getter do Adapter do DB
	 */
	public function getDbAdapter()
	{
		return $this->dbAdapter;
	}
	
	/**
	 * Seta o ID do registro
	 * @param Int $id
	 */
	public function setRecordId( $id )
	{
		$this->id = $id;
	}
	
	public function getData()
	{
		$data = array_filter( get_object_vars( $this ) );

		unset( $data['primaryKeyField'] );
		unset( $data['inputFilter'] );
		unset( $data['tableName'] );
		unset( $data['dbAdapter'] );
		
		return $data;
	}
	
    /**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
    public function getInputFilter()
    {
        if( !$this->inputFilter )
		{
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
 
            $inputFilter->add($factory->createInput(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));
 
            $inputFilter->add($factory->createInput(array(
				'name'     => 'path',
				'required' => true,
				'filters'  => array(
					//array('name' => 'FileLowercase'), // Não usar pois isto corrompe a imagem
				),
				'validators'  => array(
					array(
						'name' => 'FileSize',
						'options' => array(
							'min'	=> '10kB',
							'max'	=> '4MB',
						),
					),
					array(
						'name' => 'FileImageSize',
						'options' => array(
							'minWidth'	=> 300,
							'maxWidth'	=> 1800,
						),
					),
					array(
						'name' => 'FileMimeType',
						'options' => array(
							'mimeType'	=> array( 'image/gif,image/jpg,image/jpeg,image/png,image/x-png' )
						),
					),
				),
			)));
 
            $this->inputFilter = $inputFilter;
        }
 
        return $this->inputFilter;
    }
}