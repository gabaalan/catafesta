<?php
namespace Admin\Model;

use Core\Model\Entity;
 
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 * Entidade AuthRole
 * 
 * @category Admin
 * @package Model
 */
class Configuration extends Entity
{
    /**
     * Nome da tabela. Campo obrigatório
     * @var string
     */
    protected $tableName = 'configuracoes';
 
    /**
     * @var int
     */
    protected $id;
 
    /**
     * @var string
     */
    protected $name;
 
    /**
     * @var string
     */
    protected $key;
 
    /**
     * @var string
     */
    protected $group;
 
    /**
     * @var string
     */
    protected $value;
	
	/**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
    public function getInputFilter()
    {
        if( !$this->inputFilter )
		{
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
 
            $inputFilter->add($factory->createInput(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));
 
            $inputFilter->add($factory->createInput(array(
                'name'     => 'value',
                'required' => true,
            )));
 
            $this->inputFilter = $inputFilter;
        }
 
        return $this->inputFilter;
    }
}