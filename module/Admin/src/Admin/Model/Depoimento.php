<?php
namespace Admin\Model;
 
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Core\Model\Entity;
 
/**
 * Entidade AuthRole
 * 
 * @category Admin
 * @package Model
 */
class Depoimento extends Entity
{
    /**
     * Nome da tabela. Campo obrigatório
     * @var string
     */
    protected $tableName = 'depoimentos';
 
    /**
     * @var int
     */
    protected $id;
 
    /**
     * @var string
     */
    protected $nome;
 
    /**
     * @var string
     */
    protected $email;
	
	/**
     * @var string
     */
    protected $mensagem;
	
	/**
     * @var int
     */
    protected $aprovado;
	
    /**
     * @var string
     */
    protected $foto;
	
	/**
	 * DBAdapter
	 * @var \Zend\Db\Adapter\Adapter 
	 */
	private $dbAdapter;
	
	/**
	 * Seta o Adapter do DB
	 * @param \Zend\Db\Adapter\Adapter $dbAdapter
	 */
	public function setDbAdapter( \Zend\Db\Adapter\Adapter $dbAdapter )
	{
		$this->dbAdapter = $dbAdapter;
	}
	
	/**
	 * Getter do Adapter do DB
	 */
	public function getDbAdapter()
	{
		return $this->dbAdapter;
	}
	
	/**
	 * Seta o ID do registro
	 * @param Int $id
	 */
	public function setRecordId( $id )
	{
		$this->id = $id;
	}
	
	public function setData($data)
	{		
		parent::setData($data);
	}
	
	public function getData()
	{
		//$data = array_filter( get_object_vars( $this ) );
		
		$data = array_filter( 
			get_object_vars( $this ), 
			function( $value ) 
			{ 
				return is_null( $value ) || $value === false || $value === '' ? false : true;
			} 
		);

		unset( $data['primaryKeyField'] );
		unset( $data['inputFilter'] );
		unset( $data['tableName'] );
		unset( $data['dbAdapter'] );
		
		return $data;
	}
	
    /**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
    public function getInputFilter()
    {
        if( !$this->inputFilter )
		{
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
 
            $inputFilter->add($factory->createInput(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));
 
            $inputFilter->add($factory->createInput(array(
                'name'     => 'aprovado',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));
 
            $inputFilter->add($factory->createInput(array(
                'name'     => 'nome',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            )));
 
            $inputFilter->add($factory->createInput(array(
				'name'     => 'email',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'EmailAddress',
						'options' => array(
							'message' => 'Este e-mail é inválido',
						)
					),
				),
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name'     => 'foto',
				'required' => false,
				'filters'  => array(
					//array('name' => 'FileLowercase'), // Não usar pois isto corrompe a imagem
				),
				'validators'  => array(
					array(
						'name' => 'FileSize',
						'options' => array(
							'min'		=> '1kB',
							'max'		=> '4MB',
							'message'	=> 'O tamanho máximo para o arquivo é 4MB',
						),
					),
					array(
						'name' => 'FileImageSize',
						'options' => array(
							'minWidth'	=> 100,
							'maxWidth'	=> 1800,
							'message'	=> 'A largura máxima é 1800px e a mínima é 100px',
						),
					),
					array(
						'name' => 'FileMimeType',
						'options' => array(
							'mimeType'	=> array( 'image/gif,image/jpg,image/jpeg,image/png,image/x-png' )
						),
					),
				),
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name'     => 'mensagem',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
			)));
 
            $this->inputFilter = $inputFilter;
        }
 
        return $this->inputFilter;
    }
}