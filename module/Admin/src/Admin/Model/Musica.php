<?php
namespace Admin\Model;
 
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Core\Model\Entity;
 
/**
 * Entidade Musica
 * 
 * @category Admin
 * @package Model
 */
class Musica extends Entity
{
    /**
     * Nome da tabela. Campo obrigatório
     * @var string
     */
    protected $tableName = 'musicas';
 
    /**
     * @var int
     */
    protected $id;
 
    /**
     * @var string
     */
    protected $autor;
 
    /**
     * @var string
     */
    protected $nome;
 
    /**
     * @var string
     */
    protected $tipo;
 
    /**
     * @var string
     */
    protected $caminho;
	
    /**
     * @var string
     */
    protected $categoria;
	
	/**
	 * DBAdapter
	 * @var \Zend\Db\Adapter\Adapter 
	 */
	private $dbAdapter;
	
	public function setData($data)
	{
		parent::setData($data);
	}
	
	public function getData()
	{
		$data = array_filter( 
			get_object_vars( $this ), 
			function( $value ) 
			{ 
				return is_null( $value ) || $value === false || $value === 0 || $value === '' ? false : true;
			} 
		);

		unset( $data['primaryKeyField'] );
		unset( $data['inputFilter'] );
		unset( $data['tableName'] );
		unset( $data['dbAdapter'] );
		
		return $data;
	}
	
	/**
	 * Seta o Adapter do DB
	 * @param \Zend\Db\Adapter\Adapter $dbAdapter
	 */
	public function setDbAdapter( \Zend\Db\Adapter\Adapter $dbAdapter )
	{
		$this->dbAdapter = $dbAdapter;
	}
	
	/**
	 * Getter do Adapter do DB
	 */
	public function getDbAdapter()
	{
		return $this->dbAdapter;
	}
	
	/**
	 * Seta o ID do registro
	 * @param Int $id
	 */
	public function setRecordId( $id )
	{
		$this->id = $id;
	}
	
    /**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
	public function getInputFilter()
	{
		if( !$this->inputFilter ) 
		{			
			$inputFilter = new InputFilter();
			$factory     = new InputFactory();

			$inputFilter->add($factory->createInput(array(
				'name'     => 'id',
				'required' => true,
				'filters'  => array(
					array('name' => 'Int'),
				),
			)));

			$inputFilter->add($factory->createInput(array(
				'name'     => 'nome',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 3,
							'max'      => 200,
							'message' => 'A quantidade de caracteres mínima para o nome é 3',
						),
					),
				),
			)));

			$inputFilter->add($factory->createInput(array(
				'name'     => 'autor',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 3,
							'max'      => 200,
							'message' => 'A quantidade de caracteres mínima para o nome é 3',
						),
					),
				),
			)));

			$inputFilter->add($factory->createInput(array(
				'name'     => 'tipo',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 3,
							'max'      => 200,
							'message' => 'A quantidade de caracteres mínima para o nome é 3',
						),
					),
				),
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name'     => 'caminho',
				'required' => true,
				'filters'  => array(
					//array('name' => 'FileLowercase'), // Não usar pois isto corrompe a imagem
				),
				'validators'  => array(
					array(
						'name' => 'FileSize',
						'options' => array(
							'min'		=> '1kB',
							'max'		=> '15MB',
							'message'	=> 'O tamanho máximo para o arquivo é 4MB',
						),
					),
					array(
						'name' => 'FileMimeType',
						'options' => array(
							'mimeType'	=> array( 'audio/mpeg,audio/mpeg3,audio/x-mpeg-3' )
						),
					),
				),
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name'     => 'categoria',
				'required' => true,
				'filters'  => array(
					array('name' => 'Int'),
				),
				'validators'  => array(
					array(
						'name'	=> 'DbRecordExists',
						'options'	=> array(
							'table'		=> 'categorias',
							'field'		=> 'id',
							'adapter'	=> $this->getDbAdapter()
						)
					)
				),
			)));
			
			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}
}