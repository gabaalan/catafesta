<?php
namespace Admin\Model;
 
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Core\Model\Entity;

use Core\Helper\Hash;
 
/**
 * Entidade Product
 * 
 * @category Admin
 * @package Model
 */
class Product extends Entity
{
    /**
     * Nome da tabela. Campo obrigatório
     * @var string
     */
    protected $tableName = 'products';
 
    /**
     * @var int
     */
    protected $id;
 
    /**
     * @var string
     */
    protected $name;
	
    /**
     * @var string
     */
    protected $value;
	
    /**
     * @var int
     */
    protected $type_id;
	
	/**
	 * DBAdapter
	 * @var \Zend\Db\Adapter\Adapter 
	 */
	private $dbAdapter;
	
	public function setData($data)
	{
		parent::setData($data);
	}
	
	public function getData()
	{
		$data = array_filter( 
			get_object_vars( $this ), 
			function( $value ) 
			{ 
				return is_null( $value ) || $value === false || $value === 0 || $value === '' ? false : true;
			} 
		);

		unset( $data['primaryKeyField'] );
		unset( $data['inputFilter'] );
		unset( $data['tableName'] );
		unset( $data['dbAdapter'] );
		
		return $data;
	}
	
	/**
	 * Seta o Adapter do DB
	 * @param \Zend\Db\Adapter\Adapter $dbAdapter
	 */
	public function setDbAdapter( \Zend\Db\Adapter\Adapter $dbAdapter )
	{
		$this->dbAdapter = $dbAdapter;
	}
	
	/**
	 * Getter do Adapter do DB
	 */
	public function getDbAdapter()
	{
		return $this->dbAdapter;
	}
	
	/**
	 * Seta o ID do registro
	 * @param Int $id
	 */
	public function setRecordId( $id )
	{
		$this->id = $id;
	}
	
    /**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
	public function getInputFilter()
	{
		if( !$this->inputFilter ) 
		{			
			$inputFilter = new InputFilter();
			$factory     = new InputFactory();

			$inputFilter->add($factory->createInput(array(
				'name'     => 'id',
				'required' => true,
				'filters'  => array(
					array('name' => 'Int'),
				),
			)));

			$inputFilter->add($factory->createInput(array(
				'name'     => 'name',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 3,
							'max'      => 200,
							'message' => 'A quantidade de caracteres mínima para o nome é 3',
						),
					),
				),
			)));
			
			$inputFilter->add(
				$factory->createInput(
					array(
						'name'     => 'value',
						'required' => true,
						'filters'  => array(
							array(
								'name' => 'Zend\I18n\Filter\NumberFormat',
								'options' => array(
									'locale'	=> 'pt_BR'
								)
							),
						),
						'validators' => array(
							array(
								'name'    => 'Float',
								'options' => array(
									'locale'	=> 'en_US',
									'message'	=> 'É permitido apenas dígitos',
								),
							),
						),
					)
				)
			);
			
			$inputFilter->add($factory->createInput(array(
				'name'     => 'type_id',
				'required' => true,
				'filters'  => array(
					array('name' => 'Int'),
				),
			)));
			
			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}
}