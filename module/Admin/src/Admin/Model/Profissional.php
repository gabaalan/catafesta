<?php
namespace Admin\Model;
 
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Core\Model\Entity;
 
/**
 * Entidade Musica
 * 
 * @category Admin
 * @package Model
 */
class Profissional extends Entity
{
    /**
     * Nome da tabela. Campo obrigatório
     * @var string
     */
    protected $tableName = 'profissionais';
 
    /**
     * @var int
     */
    protected $id;
 
    /**
     * @var string
     */
    protected $telefone;
 
    /**
     * @var string
     */
    protected $nome;
 
    /**
     * @var string
     */
    protected $email;
 
    /**
     * @var string
     */
    protected $site;
	
    /**
     * @var string
     */
    protected $descricao;
	
    /**
     * @var string
     */
    protected $categoria_id;
	
	/**
	 * DBAdapter
	 * @var \Zend\Db\Adapter\Adapter 
	 */
	private $dbAdapter;
	
	public function setData($data)
	{
		parent::setData($data);
	}
	
	public function getData()
	{
		$data = array_filter( 
			get_object_vars( $this ), 
			function( $value ) 
			{ 
				return is_null( $value ) || $value === false || $value === 0 ? false : true;
			} 
		);

		unset( $data['primaryKeyField'] );
		unset( $data['inputFilter'] );
		unset( $data['tableName'] );
		unset( $data['dbAdapter'] );
		
		return $data;
	}
	
	/**
	 * Seta o Adapter do DB
	 * @param \Zend\Db\Adapter\Adapter $dbAdapter
	 */
	public function setDbAdapter( \Zend\Db\Adapter\Adapter $dbAdapter )
	{
		$this->dbAdapter = $dbAdapter;
	}
	
	/**
	 * Getter do Adapter do DB
	 */
	public function getDbAdapter()
	{
		return $this->dbAdapter;
	}
	
	/**
	 * Seta o ID do registro
	 * @param Int $id
	 */
	public function setRecordId( $id )
	{
		$this->id = $id;
	}
	
    /**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
	public function getInputFilter()
	{
		if( !$this->inputFilter ) 
		{			
			$inputFilter = new InputFilter();
			$factory     = new InputFactory();

			$inputFilter->add($factory->createInput(array(
				'name'     => 'id',
				'required' => true,
				'filters'  => array(
					array('name' => 'Int'),
				),
			)));

			$inputFilter->add($factory->createInput(array(
				'name'     => 'nome',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 3,
							'max'      => 200,
							'message' => 'A quantidade de caracteres mínima para o nome é 3',
						),
					),
				),
			)));

			
			$inputFilter->add($factory->createInput(array(
				'name'     => 'email',
				'required' => false,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'EmailAddress',
						'options' => array(
							'message' => 'Este e-mail é inválido',
						)
					),
					array(
						'name'    => 'Db_NoRecordExists',
						'options' => array(
							'message' => 'Este e-mail já foi usado nos profissionais',
							'table' => 'usuarios',
							'field' => 'email',
							'adapter' => $this->dbAdapter,
							'exclude' => array(
								'field' => 'id',
								'value' => !is_null( $this->id ) && !empty( $this->id ) ? $this->id : 0
							)
						),
					),
				),
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name'     => 'telefone',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 10,
							'max'      => 100,
							'message' => 'A quantidade de caracteres mínima é 10 e máxima é 100',
						),
					),
				),
			)));

			$inputFilter->add($factory->createInput(array(
				'name'     => 'site',
				'required' => false,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 10,
							'max'      => 250,
							'message' => 'A quantidade de caracteres mínima para o nome é 10',
						),
					),
				),
			)));

			$inputFilter->add($factory->createInput(array(
				'name'     => 'descricao',
				'required' => false,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 3,
							'max'      => 250,
							'message' => 'A quantidade de caracteres mínima para o nome é 3',
						),
					),
				),
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name'     => 'categoria_id',
				'required' => true,
				'filters'  => array(
					array('name' => 'Int'),
				),
				'validators'  => array(
					array(
						'name'	=> 'DbRecordExists',
						'options'	=> array(
							'table'		=> 'categorias',
							'field'		=> 'id',
							'adapter'	=> $this->getDbAdapter()
						)
					)
				),
			)));
			
			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}
}