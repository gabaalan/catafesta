<?php
namespace Core\Acl;
 
use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
 
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;

//use Zend\Db\Sql\Sql;
 
class Builder implements ServiceManagerAwareInterface
{
	protected static $roles = array();
	
	protected static $resources = array();
	
	protected static $privileges = array();
	
    /**
     * @var ServiceManager
     */
    protected $serviceManager;
 
    /**
     * @param ServiceManager $serviceManager
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }
 
    /**
     * Retrieve serviceManager instance
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }
 
    /**
     * Constroi a ACL
     * @return Acl 
     */
    public function build()
    {
        $acl = new Acl();
		
		$roles = $this->getRoles();
		
		foreach( $roles as $role )
		{
			$acl->addRole( new Role( $role['name'] ), $role['extend'] );
		}
		
		// ----------------------------------------------------------------------------------------

		$resources = $this->getResources();
		
		foreach( $resources as $resource )
		{
			 $acl->addResource( new Resource( $resource['path'] ) );
		}
		
		// ----------------------------------------------------------------------------------------
		
		$privileges = $this->getPrivileges();
		
		foreach( $privileges as $privilege )
		{
			if( (boolean) $privilege['allow'] )
			{
				$acl->allow( $privilege['role'], $privilege['resource'] );
			}
			else
			{
				$acl->deny( $privilege['role'], $privilege['resource'] );
			}
		}
		
        return $acl;
    }
	
	/**
	 * Retorna os roles do sistema, que são pegos no banco
	 * @return array
	 */
	protected function getRoles()
	{
		return self::$roles;
	}
	
	/**
	 * Retorna os recursos
	 * @return Array
	 */
	protected function getResources()
	{
		return self::$resources;
	}
	
	/**
	 * Retorna os privilegios dos roles
	 * @return Array
	 */
	protected function getPrivileges()
	{
		return self::$privileges;
	}
}