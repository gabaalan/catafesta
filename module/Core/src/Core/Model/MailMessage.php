<?php
namespace Core\Model;
 
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Core\Model\Entity;
 
/**
 * Entidade Config
 * 
 * @category Application
 * @package Model
 */
class MailMessage extends Entity
{
    /**
     * @var string
     */
    protected $tableName ='mail_message';
 
    /**
     * @var int
     */
    protected $id;
 
    /**
     * @var timestamp
     */
    protected $created;

    /**
     * @var timestamp
     */
    protected $planned;

    /**
     * @var string
     */
    protected $generator;
        
    /**
     * @var bool
     */
    protected $sent;
        
        /**
     * @var string
     */
    protected $from_email;
        
        /**
     * @var string
     */
    protected $from_name;

        /**
     * @var string
     */
    protected $replyto_email;
  
        /**
     * @var string
     */
    protected $replyto_name;

        
        /**
     * @var string
     */
    protected $to_email;
        
        /**
     * @var string
     */
    protected $cc_email;
        
        /**
     * @var string
     */
    protected $bcc_email;
        
        /**
     * @var string
     */
    protected $subject;
        
        /**
     * @var text
     */
    protected $body;
        
        /**
     * @var text
     */
    protected $format;
        
        
        /**
     *
     * @return Zend\InputFilter\InputFilter
     */
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
 
            $inputFilter->add($factory->createInput(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));
 
            $inputFilter->add($factory->createInput(array(
                'name'     => 'created',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'Date',
                                                'options' => array(
                                                        'format' => "Y-m-d H:i:s",
                                                        //'locale' => 'pt_BR'
                                                )
                    ),
                ),
            )));
                        
            $inputFilter->add($factory->createInput(array(
                'name'     => 'planned',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'Date',
                                                'options' => array(
                                                        'format' => "Y-m-d H:i:s",
                                                        //'locale' => 'pt_BR'
                                                )
                    ),
                ),
            )));
                        
            $inputFilter->add($factory->createInput(array(
                'name'     => 'generator',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            )));
 
                        $inputFilter->add($factory->createInput(array(
                                'name'     => 'from_email',
                                'required' => true,
                                'filters'  => array(
                                        array('name' => 'StripTags'),
                                        array('name' => 'StringTrim'),
                                ),
                                'validators' => array(
                                        array(
                                                'name'    => 'EmailAddress',
                                        ),
                                ),
                        )));
                        
            $inputFilter->add($factory->createInput(array(
                'name'     => 'from_name',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            )));
            
                        $inputFilter->add($factory->createInput(array(
                                'name'     => 'replyto_email',
                                'required' => true,
                                'filters'  => array(
                                        array('name' => 'StripTags'),
                                        array('name' => 'StringTrim'),
                                ),
                                'validators' => array(
                                        array(
                                                'name'    => 'EmailAddress',
                                        ),
                                ),
                        )));                    
                        
                        $inputFilter->add($factory->createInput(array(
                'name'     => 'replyto_name',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            )));
                        
                        $inputFilter->add($factory->createInput(array(
                                'name'     => 'to_email',
                                'required' => true,
                                'filters'  => array(
                                        array('name' => 'StripTags'),
                                        array('name' => 'StringTrim'),
                                ),
                                'validators' => array(
                                        array(
                                                'name'    => 'EmailAddress',
                                        ),
                                ),
                        )));
            
                        $inputFilter->add($factory->createInput(array(
                                'name'     => 'cc_email',
                                'required' => false,
                                'filters'  => array(
                                        array('name' => 'StripTags'),
                                        array('name' => 'StringTrim'),
                                ),
                        )));
                        
                        $inputFilter->add($factory->createInput(array(
                                'name'     => 'bcc_email',
                                'required' => false,
                                'filters'  => array(
                                        array('name' => 'StripTags'),
                                        array('name' => 'StringTrim'),
                                ),
                        )));
                
            $inputFilter->add($factory->createInput(array(
                'name'     => 'subject',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            )));
                        
                        $inputFilter->add($factory->createInput(array(
                'name'     => 'body',
                'required' => true,
                'filters'  => array(
                ),
            )));
                        
                        $inputFilter->add($factory->createInput(array(
                'name'     => 'format',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            )));
 
            $this->inputFilter = $inputFilter;
        }
 
        return $this->inputFilter;
    }
}