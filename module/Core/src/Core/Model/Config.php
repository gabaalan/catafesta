<?php
namespace Core\Model;
 
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Core\Model\Entity;
 
/**
 * Entidade Config
 * 
 * @category Application
 * @package Model
 */
class Config extends Entity
{
    /**
     * Nome da tabela. Campo obrigatório
     * @var string
     */
    protected $tableName ='config';
 
    /**
     * @var int
     */
    protected $id;
 
    /**
     * @var string
     */
    protected $group;

    /**
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $value;
        
    /**
     * @var string
     */
    protected $description;
  
}