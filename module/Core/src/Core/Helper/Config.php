<?php

namespace Core\Helper;

use Core\Db\TableGateway;

/**
 * Classe para centralizar a geração da hash da senha e activation code
 *
 * @author Gabriel
 */
class Config
{
	protected $dbAdapter;
	
	public function __construct( \Zend\Db\Adapter\Adapter $dbAdapter )
	{
		$this->dbAdapter = $dbAdapter;
	}
	
	public function get( $key )
	{
		$config = $this->getTable('Core\Model\Config')->select( array( 'key' => $key ) )->current();
		
		if( $config )
			return $config->value;
		
		return null;
	}
	
	protected function getTable( $table )
    {
        $tableGateway = new TableGateway($this->dbAdapter, $table, new $table);
        $tableGateway->initialize();

        return $tableGateway;
    }
}
