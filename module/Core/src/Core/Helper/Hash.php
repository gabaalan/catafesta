<?php

namespace Core\Helper;

/**
 * Classe para centralizar a geração da hash da senha e activation code
 *
 * @author Gabriel
 */
class Hash
{
	/**
	 * Retorna um id único
	 * @return String ID unico que retorna da função uniqid do PHP
	 */
	public static function getUniqueId()
	{
		return uniqid( rand(), true );
	}
	
	/**
	 * Retorna a HASH do Password que é gravada no banco
	 * @param String $password
	 * @return String
	 */
	public static function getPasswordHash( $password )
	{
		return md5( 'LTLoJHACP8rcT83Z' . $password );
	}
	
	/**
	 * Retorna uma senha randomica
	 * @param Int $length tamanho da senha
	 * @param String $salt Tipo da senha
	 * @return String
	 */
	public static function getRandomPassword( $length = 8, $salt = 'alpha' )
	{
		if( $salt == 'alpha' )
		{
				$salt = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		} 
		else if( $salt == 'int')
		{
				$salt = "123456789";
		}

		$len		= strlen($salt);
		$makepass	= '';

		$stat = @stat(__FILE__);
		
		if( empty( $stat ) || !is_array( $stat ) ) 
			$stat = array(php_uname());

		mt_srand(crc32(microtime() . implode('|', $stat)));

		for ($i = 0; $i < $length; $i ++)
		{
				$makepass .= $salt[mt_rand(0, $len -1)];
		}

		return $makepass;
	}
}
