<?php
 
namespace Core\View\Helper;
 
use Zend\View\Helper\AbstractHelper;
 
/**
 * Helper que permite pegar um Service na View
 * 
 * @category Application
 * @package View\Helper
 * @author  Gaba
 */
class Truncate extends AbstractHelper
{
	private function truncate( $string, $limit, $break = '.', $pad = '...' )
	{
		if( strlen( $string ) <= $limit )
			return $string;
		
		if( false !== ( $breakpoint = strpos( $string, $break, $limit ) ) )
		{
			if( $breakpoint < strlen( $string ) - 1) 
			{
				$string = substr( $string, 0, $breakpoint ) . $pad;
			}
		}
		
		return $string;
	}
	
	public function __invoke( $string, $limit, $break = '.', $pad = '...' )
    {
        return $this->truncate( $string, $limit, $break, $pad );
    }
}