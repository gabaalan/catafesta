<?php
 
namespace Core\View\Helper;
 
use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
 
/**
 * Helper que permite pegar um Service na View
 * 
 * @category Application
 * @package View\Helper
 * @author  Gaba
 */
class Service extends AbstractHelper implements ServiceLocatorAwareInterface
{
    /**
     * Set the service locator.
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return CustomHelper
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }
    /**
     * Get the service locator.
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
	
    public function __invoke()
    {
        $helperPluginManager	= $this->getServiceLocator();
        $serviceManager			= $helperPluginManager->getServiceLocator();
		
        return $serviceManager;
    }
}