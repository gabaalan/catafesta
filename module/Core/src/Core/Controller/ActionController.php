<?php
namespace Core\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\ServiceManager\Exception\ServiceNotFoundException;

use Zend\Mail\Message;
use Zend\Mail\Transport\Sendmail as SendmailTransport;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

use Core\Db\TableGateway;
use Core\Model\MailMessage;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

class ActionController extends AbstractActionController
{
	/**
     * Create a mail message
     *
     * @param  array (priority, planned, generator, from_email, from_name, replyto_email, to_email, cc_email, bcc_email, subject, body)
     * @return Bool
     */
	protected function genMail($maildata)
    {
		$mailmessage = new MailMessage; 

		$maildata['created'] = date("Y-m-d H:i:s");             
		
		$configuration = $this->getTable('Admin\Model\Configuration');

		if( !isset( $maildata['from_email'] ) ) 
		{
			$maildata['from_email'] = $configuration->getRowByKey('key', 'email_smtp_sender_email')->value;
		}
		
		if( !isset( $maildata['from_name'] ) ) 
		{
			$maildata['from_name'] = $configuration->getRowByKey('key', 'email_smtp_sender_name')->value;
		}
		
		if( !isset( $maildata['replyto_email'] ) ) 
		{
			$maildata['replyto_email'] = $configuration->getRowByKey('key', 'email_notification')->value;
		}
		
		if( !isset( $maildata['replyto_name'] ) ) 
		{
			$maildata['replyto_name'] = $configuration->getRowByKey('key', 'email_smtp_sender_name')->value;
		}     

		if( !isset( $maildata['planned'] ) )
		{
			$maildata['planned'] = date("Y-m-d H:i:s", time() + ( (30*60) * $maildata['priority'] ) );
		}

		$mailmessage->setData($maildata);
		$this->getTable('Core\Model\MailMessage')->save( $mailmessage );
		
		if( $maildata['priority'] == 0 )
		{
			$this->sendMailMessages();
		}

		return true;
    }
	
	protected function sendMailMessages()
	{
		set_time_limit( 120 );
		
		$configuration = $this->getTable('Admin\Model\Configuration');

		$mail_qty_sent		= 20;
		$mail_sleep_time	= 5;
		$mail_sleep_qty		= 10;

		$hoje = new \DateTime();
		$hoje->add( new \DateInterval('PT5M') );

		$msgs = $this->getTable('Core\Model\MailMessage')->select(
			function( Select $select ) use ($hoje)
			{
				$select	
					->where( array( 'sent' => 0 ) )
					->where( "planned <= TIMESTAMP('" . $hoje->format('Y-m-d H:i:s') . "', 'YYYY-MM-DD HH24:MI:SS')" )
					->order('mail_message.planned DESC')
					;
			}
		)->toArray();

		$sentMsgs = '';
		
		if( $msgs )
		{
			$mail_system = $where = 'smtp';
			$mail_default_sender_name	= $configuration->getRowByKey('key', 'email_smtp_sender_name')->value;
			$mail_default_sender_email	= $configuration->getRowByKey('key', 'email_smtp_sender_email')->value;
			$mail_smtp_host = $configuration->getRowByKey('key', 'email_smtp_host')->value;
			$mail_smtp_port = $configuration->getRowByKey('key', 'email_smtp_port')->value;
			$mail_smtp_ssl = $configuration->getRowByKey('key', 'email_smtp_ssl')->value;
			$mail_smtp_auth = $configuration->getRowByKey('key', 'email_smtp_auth')->value;
			$mail_smtp_user = $configuration->getRowByKey('key', 'email_smtp_user')->value;
			$mail_smtp_password = $configuration->getRowByKey('key', 'email_smtp_password')->value;
			$mail_default_format = 'text';

			if( $mail_system == 'sendmail' )
			{
				$transport = new SendmailTransport();
			} 
			else if ( $mail_system == 'smtp' )
			{
				$transport = new SmtpTransport();
				$options   = new SmtpOptions(array(
						'name'              => 'localhost',
						'host'              => $mail_smtp_host,
						'port'              => $mail_smtp_port, // Notice port change for TLS is 587
						'connection_class'  => 'login',
						'connection_config' => array(
								'username' => $mail_smtp_user,
								'password' => $mail_smtp_password,
						//      'ssl'      => $mail_smtp_ssl,
						),
				));
				$transport->setOptions($options);
			}

			$iMsg = 0;

			foreach( $msgs as $msg )
			{
				if( $iMsg  == $mail_sleep_qty )
				{ 
					$iMsg = 0;
					sleep( $mail_sleep_time );
				}

				$message = new Message();
				$message->addFrom($msg['from_email'], $msg['from_name'])
								->addTo($msg['to_email'])
								->setSubject($msg['subject']);

				if ( isset( $msg['cc_email'] )) {$message->addCc($msg['cc_email']);}
				if ( isset( $msg['bcc_email'] )) {$message->addBCc($msg['bcc_email']);}

				$message->addReplyTo($msg['replyto_email'], $msg['replyto_name']);

				$message->setEncoding("UTF-8");

				if( $msg['format'] == 'html' )
				{
					$textContent = '';
					
					$text = new MimePart($textContent);
					$text->type = "text/plain";

					$html = new MimePart($msg['body']);
					$html->type = "text/html";

					$body = new MimeMessage();
					$body->setParts(
						array( $text, $html )
					);

					$message->setBody($body);
				} 
				else
				{
					$message->setBody($msg['body']);
				}                               

				$transport->send($message);                             
				$sentMsgs .= $msg['planned'] . " " . $msg['to_email'] . " " . $msg['generator'] . "<br>";

				$this	->getTable('Core\Model\MailMessage')
						->updateValueByKey('sent', 1, 'id', $msg['id'] );

				$iMsg++;        
			}
		} 
		else 
		{
			$sentMsgs = 'No message to be sent!';
		}

		return $sentMsgs;               
    }
        
	public function getBaseUrl()
	{
		$uri = $this->getRequest()->getUri();
		$scheme = $uri->getScheme();
		$host = $uri->getHost();
		$base = sprintf('%s://%s', $scheme, $host);

		return $base;
	}
	
	/**
	 * Returns a Configuratin Value
	 *
	 * @param  string $key
	 * @return Value
	 */
	protected function getConfig($keyName)
	{
		$config = $this->getTable('Core\Model\Config')->getValueByKey('value','key',$keyName);
				
		return $config;
	}
	
	/**
     * Returns a TableGateway
     *
     * @param  string $table
     * @return TableGateway
     */
	protected function getTable( $table )
    {
        $sm = $this->getServiceLocator();
        $dbAdapter = $sm->get('DbAdapter');
		
		$object = new $table;

		if( $sm->has( "Db\\" .  $table ) )
			$object = $sm->get( "Db\\" .  $table );
		
        $tableGateway = new TableGateway($dbAdapter, $table, $object);
        $tableGateway->initialize();
		
        return $tableGateway;
    }

    /**
     * Returns a Service
     *
     * @param  string $service
     * @return Service
     */
    protected function getService($service)
    {
        return $this->getServiceLocator()->get($service);
    }
}