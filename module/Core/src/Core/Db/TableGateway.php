<?php

namespace Core\Db;

use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Select;
use Core\Model\EntityException;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;

class TableGateway extends AbstractTableGateway implements EventManagerAwareInterface
{
    /**
     * Primary Key field name
     *
     * @var string
     */
    protected $primaryKeyField;

    /**
     * ObjectPrototype
     * @var stdClass
     */
    protected $objectPrototype;

	/**
	 *
	 * @var EventManagerInterface
	 */
	protected $events;
	
	/**
	 * List of events
	 */
	const EVENT_POS_INSERT = 'posInsert';
	const EVENT_PRE_INSERT = 'preInsert';
	const EVENT_POS_UPDATE = 'posUpdate';
	const EVENT_PRE_UPDATE = 'preUpdate';
	const EVENT_POS_SAVE = 'posSave';
	const EVENT_PRE_SAVE = 'preSave';

    public function setEventManager(EventManagerInterface $events)
    {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class(),
        ));
        $this->events = $events;
        return $this;
    }

    public function getEventManager()
    {
        if (null === $this->events) {
            $this->setEventManager(new EventManager());
        }
        return $this->events;
    }
	
    public function __construct(Adapter $adapter, $table, $objectPrototype)
    {
        $this->adapter = $adapter;
        $this->table = $objectPrototype->getTableName();
        $this->objectPrototype = $objectPrototype;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype($objectPrototype);
		
		$this->attachEvents();
    }

	/**
	 * Attach events to the gateway
	 */
	protected function attachEvents()
	{
		$preInsert = array( $this->objectPrototype, self::EVENT_PRE_INSERT );
		$posInsert = array( $this->objectPrototype, self::EVENT_POS_INSERT );
		$preUpdate = array( $this->objectPrototype, self::EVENT_PRE_UPDATE );
		$posUpdate = array( $this->objectPrototype, self::EVENT_POS_UPDATE );
		$preSave = array( $this->objectPrototype, self::EVENT_PRE_SAVE );
		$posSave = array( $this->objectPrototype, self::EVENT_POS_SAVE );
		
		if( is_callable( $preInsert ) )
			$this->getEventManager()->attach( self::EVENT_PRE_INSERT, $preInsert );
		
		if( is_callable( $posInsert ) )
			$this->getEventManager()->attach( self::EVENT_POS_INSERT, $posInsert );
		
		if( is_callable( $preInsert ) )
			$this->getEventManager()->attach( self::EVENT_PRE_UPDATE, $preUpdate );
		
		if( is_callable( $preInsert ) )
			$this->getEventManager()->attach( self::EVENT_POS_UPDATE, $posUpdate );
		
		if( is_callable( $preInsert ) )
			$this->getEventManager()->attach( self::EVENT_PRE_SAVE, $preSave );
		
		if( is_callable( $preInsert ) )
			$this->getEventManager()->attach( self::EVENT_POS_SAVE, $posSave );
	}

	public function initialize()
    {
        parent::initialize();

        $this->primaryKeyField = $this->objectPrototype->primaryKeyField;
        if ( ! is_string($this->primaryKeyField)) {
            $this->primaryKeyField = 'id';
        }
    }   
    
    public function fetchAll($columns = null, $where = null, $limit = null, $offset = null)
    {
        $select = new Select();
        $select->from($this->getTable());

        if ($columns)
            $select->columns($columns);

        if ($where)
            $select->where($where);

        if ($limit)
            $select->limit((int) $limit);

        if ($offset)
            $select->offset((int) $offset);

        return $this->selectWith($select);
    }

    public function get($id)
    {
        $id  = (int) $id;
        $rowset = $this->select(array($this->primaryKeyField => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new EntityException("Could not find row $id");
        }
        return $row;
    }

    public function save($object)
    {
        $data	= $object->getData();

        $id		= (int) isset($data[$this->primaryKeyField]) ? $data[$this->primaryKeyField] : 0;
		
		if( $id == 0 ) 
		{
			// Fire events pre insert and pre save
			$this->getEventManager()->trigger( 'preInsert', null, array( 'dbAdapter' => $this->adapter, 'data' => $data ) );
			$this->getEventManager()->trigger( 'preSave', null, array( 'dbAdapter' => $this->adapter, 'data' => $data ) );
			
			if ($this->insert($data) < 1)
				throw new EntityException("Erro ao inserir", 1);

			// Fire event pos insert
			$this->getEventManager()->trigger( 'posInsert', null, array( 'dbAdapter' => $this->adapter, 'id' => $this->lastInsertValue, 'data' => $data ) );
			
			$object->id = $this->lastInsertValue;
		} 
		else 
		{
			if( !$this->get( $id ) )
				throw new EntityException('Id does not exist');
			
			// Fire event pre update
			$this->getEventManager()->trigger( 'preUpdate', null, array( 'dbAdapter' => $this->adapter, 'id' => $id, 'data' => $data ) );
			
			try
			{
				$this->update($data, array($this->primaryKeyField => $id));
			}
			catch( \Exception $excecao )
			{
				throw $excecao;
			}
			
			//if( $rowsAffected < 1 )
				//throw new EntityException("Erro ao atualizar", 1);
			
			// Fire event pos update
			$this->getEventManager()->trigger( 'posUpdate', null, array( 'dbAdapter' => $this->adapter, 'id' => $id, 'data' => $data ) );
		}
		
		// Fire event pos save
		$this->getEventManager()->trigger( 'posSave', null, array( 'dbAdapter' => $this->adapter, 'id' => $id, 'data' => $data ) );
		
        return $object;
    }

    public function delete($id)
    {
        return parent::delete(array($this->primaryKeyField => $id));
    }
	
	public function getValueByKey($ColumnValue, $ColumnKey, $keyName)
    {
                // select $ColumnValue from $table where $ColumnKey = $keyName
        $rowset = $this->select(array($ColumnKey => $keyName));
        $row = $rowset->current()->$ColumnValue;
                
        return $row;
    }
        
	public function getRowByKey($ColumnKey, $value)
    {
		// select * from x where $ColumnKey = $value
        $rowset = $this->select(array($ColumnKey => $value));
        $row = $rowset->current();
                
        return $row;
    }
        
	public function updateValueByKey($Column, $ColumnValue, $Key, $KeyValue)
    {
		//update table set $Column = $ColumnValue where $Key = $KeyValue 
		$this->update(array($Column => $ColumnValue), array($Key => $KeyValue));                
        return true;
    }
        
        
	public function insertDB($object)
    {

        $this->data = $object->getData( true );
        
//		$this->preInsert();     
		$this->save($this->data);
//		$this->postInsert();    
        
        $object->id = $this->lastInsertValue;
        
        return $object;
    }
        

	public function updateDB($object)
    {
                
                //print_r($object);die;
                
                $data = $object->getData( false );
                
                //print_r($data);die;
                
        $id = (int) isset($data[$this->primaryKeyField]) ? $data[$this->primaryKeyField] : 0;
        
                if ($id == 0) {
            
                        throw new EntityException("Error on Update (no id found)", 1);
                
        } else {
            if (! $this->get($id)) 
                throw new EntityException('Id does not exist');
            if ($this->update($data, array($this->primaryKeyField => $id)) < 1)
                throw new EntityException("Error on Updates", 1);
        }
                
        return $data;
                
    }
}