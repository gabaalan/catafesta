<?php
return array(
	'di' => array(),
	'view_helpers'	=> array(
		'invokables'	=> array(
			'session'			=> 'Core\View\Helper\Session',
			'service'			=> 'Core\View\Helper\Service',
			'truncate'			=> 'Core\View\Helper\Truncate',
		)
	)
);
