<?php

// module/Application/conﬁg/module.config.php:
return array(
    'controllers' => array( //add module controllers
        'invokables' => array(
            'Application\Controller\Index'			=> 'Application\Controller\IndexController',
            'Application\Controller\Quem'			=> 'Application\Controller\QuemController',
            'Application\Controller\Atuacao'		=> 'Application\Controller\AtuacaoController',
            'Application\Controller\Fotos'			=> 'Application\Controller\FotosController',
            'Application\Controller\Videos'			=> 'Application\Controller\VideosController',
            'Application\Controller\Profissionais'	=> 'Application\Controller\ProfissionaisController',
            'Application\Controller\Contato'		=> 'Application\Controller\ContatoController',
            'Application\Controller\Repertorio'		=> 'Application\Controller\RepertorioController',
            'Application\Controller\Depoimentos'		=> 'Application\Controller\DepoimentosController',
        ),
		
    ),

    'router' => array(
        'routes' => array(
            'home' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                        'module'        => 'application'
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                        'child_routes' => array( //permite mandar dados pela url 
                            'wildcard' => array(
                                'type' => 'Wildcard'
                            ),
                        ),
                    ),
                ),
            ),
			'depoimentos' => array(
				'type'    => 'Segment',
                'options' => array(
                    'route'    => '/depoimentos[/:action][/:page]',
					'constraints' => array(
						'action'	=> '[a-zA-Z][a-zA-Z0-9_-]*',
						'page'		=> '[0-9]*',
					),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Depoimentos',
                        'action'        => 'index',
                        'module'        => 'application',
                    ),
                ),
            ),
			'quem' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/quem-somos',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Quem',
                        'action'        => 'index',
                        'module'        => 'application',
                    ),
                ),
            ),
			'atuacao' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/atuacao',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Atuacao',
                        'action'        => 'index',
                        'module'        => 'application',
                    ),
                ),
            ),
			'fotos' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/fotos[/:page]',
					'constraints' => array(
						'page'		=> '[0-9]*',
					),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Fotos',
                        'action'        => 'index',
                        'module'        => 'application',
                    ),
                ),
            ),
			'videos' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/videos',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Videos',
                        'action'        => 'index',
                        'module'        => 'application',
                    ),
                ),
            ),
			'profissionais' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/profissionais',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Profissionais',
                        'action'        => 'index',
                        'module'        => 'application',
                    ),
                ),
            ),
			'contato' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/orcamento',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Contato',
                        'action'        => 'index',
                        'module'        => 'application',
                    ),
                ),
            ),
			'repertorio' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/repertorio',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Repertorio',
                        'action'        => 'index',
                        'module'        => 'application',
                    ),
                ),
            ),
        ),
    ),
	
	'service_manager' => array(
		'factories' => array(
			'translator'	=> 'Zend\I18n\Translator\TranslatorServiceFactory',
			'navigation'	=> 'Zend\Navigation\Service\DefaultNavigationFactory',
		),
    ),
					
	'translator' => array(
		'locale' => 'pt_BR',
		'translation_file_patterns' => array(
			array(
				'type'     => 'phparray', //'gettext',
				'base_dir' => __DIR__ . '/../language',
				'pattern'  => '%s.php', //'%s.mo',
			),
		),
	),
					
	'view_helpers'	=> array(
		'invokables'	=> array(
		)
	),
	
	'view_manager' => array(
		'display_not_found_reason' => true,
		'display_exceptions'       => true,
		'doctype'                  => 'HTML5',
		'not_found_template'       => 'application/error/404',
		'exception_template'       => 'application/error/index',
		'template_map' => array(
			'layout/layout'				=> realpath( __DIR__ . '/../view/layout/layout.phtml' ),
			'application/error/404'		=> realpath( __DIR__ . '/../view/error/404.phtml' ),
			'application/error/index'	=> realpath( __DIR__ . '/../view/error/index.phtml' ),
		),
		'template_path_stack' => array(
			'application' => __DIR__ . '/../view',
		),
		'strategies' => array(
            'ViewJsonStrategy',
        ),
	),
				
	'navigation'	=> array(
		'default'	=> array(
			array(
				'label'		=> 'Home',
				'route'		=> 'home',
				'controller'=> 'Application\Controller\Index',
				'action'	=> 'index',
			),
			array(
				'label'		=> 'Quem Somos',
				'route'		=> 'quem',
				'controller'=> 'Quem',
				'action'	=> 'index',
			),
			array(
				'label'		=> 'Depoimentos',
				'route'		=> 'depoimentos',
				'controller'=> 'Depoimentos',
				'action'	=> 'index',
			),
			array(
				'label'		=> 'Fotos',
				'route'		=> 'fotos',
				'controller'=> 'Fotos',
				'action'	=> 'index',
			),
			array(
				'label'		=> 'Repertório',
				'route'		=> 'repertorio',
				'controller'=> 'Repertorio',
				'action'	=> 'index',
			),
			array(
				'label'		=> 'Vídeos',
				'route'		=> 'videos',
				'controller'=> 'Videos',
				'action'	=> 'index',
			),
			array(
				'label'		=> 'Profissionais',
				'route'		=> 'profissionais',
				'controller'=> 'Profissionais',
				'action'	=> 'index',
			),
			array(
				'label'		=> 'Orçamento',
				'route'		=> 'contato',
				'controller'=> 'Contato',
				'action'	=> 'index',
			),
		)
	),
);