<?php

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Authentication\AuthenticationService;
use Zend\Mvc\MvcEvent;

use Core\Db\TableGateway;
use Admin\Model\Musica;
use Admin\Model\Configuration;

class Module
{
	protected $_flashMessenger;
	
	/**
	 * Executada no bootstrap do módulo
	 * @param MvcEvent $event
	 */
	public function onBootstrap( $event )
	{
		$serviceManager = $event->getApplication()->getServiceManager();
		
		/** @var \Zend\ModuleManager\ModuleManager $moduleManager */
		$moduleManager = $serviceManager->get('modulemanager');
		
		$translator = $serviceManager->get('translator');
		
		\Zend\Validator\AbstractValidator::setDefaultTranslator( new \Zend\Mvc\I18n\Translator( $translator ) );
		
		$eventManager        = $event->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
		
		/** @var \Zend\EventManager\SharedEventManager $sharedEvents */
		$sharedEvents = $moduleManager->getEventManager()->getSharedManager();

		//adiciona eventos ao módulo - 'Zend\Mvc\Controller\AbstractActionController' <- Geral
		$sharedEvents->attach(__NAMESPACE__, MvcEvent::EVENT_DISPATCH, array($this, 'setLayout'), 99);
		$sharedEvents->attach(__NAMESPACE__, MvcEvent::EVENT_DISPATCH, array($this, 'mvcPreDispatch'), 100);
		
		//$eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, array($this, 'onDispatchError'), 100);		
    }
	
	public function mvcPreDispatch( MvcEvent $event )
	{
		$serviceLocator	= $event->getTarget()->getServiceLocator();
		//$routeMatch		= $event->getRouteMatch();
		//$moduleName		= $routeMatch->getParam('module');

		$dbAdapter = $serviceLocator->get('DbAdapter');
		
		$object = new Musica();
		
        $tableGateway = new TableGateway( $dbAdapter, 'Admin\Model\Musica', $object );
        $tableGateway->initialize();
		
        $sql = $tableGateway->getSql();
		
		$select = $sql->select();
		$select
			->columns( array( 'id', 'autor', 'nome', 'caminho' ) )
			->order('nome ASC')
			->limit(150)
			;
		
		$statement = $sql->prepareStatementForSqlObject( $select );
		
		$musicas = $statement->execute();
		
		// ----------------------------------------------------------------------------------------
		
		$viewModel = $event->getViewModel();
		
		$viewModel->radio = $musicas;
		
		// ----------------------------------------------------------------------------------------
		
		$viewModel->endereco = $this->getConfiguration( 'rodape_endereco', $dbAdapter );
		
		return true;
	}
	
	private function getConfiguration( $key, $dbAdapter )
	{
		if( !$key )
			return null;
		
		$object = new Configuration();
		
        $tableGateway = new TableGateway( $dbAdapter, 'Admin\Model\Configuration', $object );
        $tableGateway->initialize();
		
        $registro = $tableGateway->getRowByKey( 'key', $key );
		
		if( $registro )
			return $registro->value;
		
		return null;
	}
	
//	public function onDispatchError( MvcEvent $event )
//	{
//		$viewModel = $event->getViewModel();
//		$viewModel->setTemplate('layout/layout');
//	}

	public function setLayout( $event )
	{
		$controller = $event->getTarget();
		$controller->layout('layout/layout');
	}
	
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
}