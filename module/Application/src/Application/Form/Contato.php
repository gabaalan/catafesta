<?php
namespace Application\Form;
 
use Zend\Form\Form;
use Zend\Form\Element;

use Core\Helper\Hash;

class Contato extends Form
{
    public function __construct( $options = array() )
    {
        parent::__construct('post');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form');
        $this->setAttribute('action', '/');
        
		// ------------------------------------------------------------------------

		$name = new Element\Text('nome');
		$name
			->setLabel('Nome')
			->setAttributes(array(
				'size'			=> '255',
				'placeholder'	=> 'João da Silva',
				'class'			=> 'form-control',
				'required'		=> 'true',
			));
		
		$email = new Element\Text('email');
		$email
			->setLabel('E-mail')
			->setAttributes(array(
				'size'			=> '200',
				'required'		=> 'true',
				'placeholder'	=> 'joao@exemplo.com',
				'class'			=> 'form-control'
			));
		
		$telefone = new Element\Text('telefone');
		$telefone
			->setLabel('Telefone')
			->setAttributes(array(
				'size'			=> '200',
				'required'		=> 'true',
				'placeholder'	=> '(99) 9999-9999',
				'class'			=> 'form-control phone'
			));
		
		$cidade = new Element\Text('cidade');
		$cidade
			->setLabel('Cidade')
			->setAttributes(array(
				'size'			=> '200',
				'required'		=> 'true',
				'placeholder'	=> 'Joinville SC',
				'class'			=> 'form-control'
			));
		
		$local = new Element\Text('local');
		$local
			->setLabel('Local do evento')
			->setAttributes(array(
				'size'			=> '200',
				'required'		=> 'true',
				'placeholder'	=> 'Endereço',
				'class'			=> 'form-control'
			));
		
		$tipo = new Element\Select('tipo');
		$tipo	->setLabel('Tipo de local')
				->setAttributes(array(
					'class'		=> 'form-control',
					'required'	=> 'true',
				));
		
		$tipo_options = array(
			'Igreja' => 'Igreja',
			'Salão' => 'Salão',
			'Sítio' => 'Sítio',
			'Local Fechado' => 'Local Fechado',
			'Local Aberto' => 'Local Aberto',
		);
		
		$tipo->setValueOptions( $tipo_options );
		
		$conheceu = new Element\Select('conheceu');
		$conheceu	->setLabel('Como nos conheceu')
				->setAttributes(array(
					'class'		=> 'form-control',
					'required'	=> 'true',
				));
		
		$conheceu_options = array(
			'Site' => 'Site',
			'Revista' => 'Revista',
			'Indicação' => 'Indicação',
			'Feiras' => 'Feiras',
			'Redes Sociais' => 'Redes Sociais',
		);
		
		$conheceu->setValueOptions( $conheceu_options );
		
		$data = new Element\Text('data');
		$data
			->setLabel('Data')
			->setAttributes(array(
				'size'			=> '200',
				'required'		=> 'true',
				'placeholder'	=> '99/99/9999',
				'class'			=> 'form-control date'
			));
		
		$horario = new Element\Text('horario');
		$horario
			->setLabel('Horário')
			->setAttributes(array(
				'size'			=> '200',
				'required'		=> 'true',
				'placeholder'	=> '99:99',
				'class'			=> 'form-control hour'
			));
		
		$formacao = new Element\MultiCheckbox('formacao');
		$formacao->setLabel('Escolha a formação')
				->setValue('true')
				/*->setValueOptions(array(
					array( 'value' => '1', 'label' => 'Sim', 'label_attributes' => array('class' => 'checkbox-inline'), ),
					array( 'value' => '0', 'label' => 'Não', 'label_attributes' => array('class' => 'radio inline'), ),
				))*/
				;
		
		$mensagem = new Element\Textarea('mensagem');
		$mensagem
			->setLabel('Mensagem')
			->setAttributes(array(
				'rows'	=> 4,
				'class'	=> 'form-control editor',
			));

		// ------------------------------------------------------------------------
		
		$this->add( $name );
		$this->add( $email );
		$this->add( $telefone );
		$this->add( $cidade );
		$this->add( $local );
		$this->add( $data );
		$this->add( $tipo );
		$this->add( $conheceu );
		$this->add( $horario );
		$this->add( $mensagem );
		$this->add( $formacao );
    }
	
	public function bind( $object, $flags = \Zend\Form\FormInterface::VALUES_NORMALIZED ) 
	{		
		parent::bind( $object, $flags );
	}
	
	public function setData( $data ) 
	{		
		if( isset( $data['submit'] ) )
			unset( $data['submit'] );
		
		parent::setData($data);
	}
}