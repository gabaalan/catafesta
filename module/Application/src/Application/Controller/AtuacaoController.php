<?php
namespace Application\Controller;
 
use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
 
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Predicate\Predicate;

/**
 * Pagina inicial
 * 
 * @category Application
 * @package Controller
 * @author  Gaba
 */
class AtuacaoController extends ActionController
{
	public function indexAction()
	{
		$texto = $this->getTable('Admin\Model\Configuration')->getRowByKey( 'key', 'pagina_atuacao' );
		
		return new ViewModel(
			array(
				'texto'	=> $texto ? $texto->value : false,
			)
		);
	}
}