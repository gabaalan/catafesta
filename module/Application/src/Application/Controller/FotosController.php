<?php
namespace Application\Controller;
 
use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
 
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Predicate\Predicate;

/**
 * Pagina inicial
 * 
 * @category Application
 * @package Controller
 * @author  Gaba
 */
class FotosController extends ActionController
{
	public function indexAction()
	{
		$sql = $this->getTable('Admin\Model\Foto')->getSql();
		
		$registros = $sql->select();
		$registros
			->order('id DESC')
			;
		
		$paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect( $registros, $sql );
		$paginator = new \Zend\Paginator\Paginator( $paginatorAdapter );
		$paginator->setItemCountPerPage( 40 ); //Default 10
		$paginator->setCurrentPageNumber( $this->params()->fromRoute('page') );

		return array(
			'registros' => $paginator,
		);
	}
}