<?php
namespace Application\Controller;
 
use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
 
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

use Application\Form\Contato;

/**
 * Pagina inicial
 * 
 * @category Application
 * @package Controller
 * @author  Gaba
 */
class ContatoController extends ActionController
{
	private $inputFilter = null;
	
	public function indexAction()
	{
		$form = new Contato();
		
		$sql = $this->getTable('Admin\Model\Musico')->getSql();
		
		$select = $sql->select();
		$select
			->order('nome ASC')
			;
		
		$statement = $sql->prepareStatementForSqlObject( $select );
		
		$musicos = $statement->execute();
		
		$options = array();
		
		foreach( $musicos as $musico )
		{
			$options[] = array( 'value' => $musico['nome'], 'label' => $musico['nome'], 'label_attributes' => array('class' => 'checkbox-inline musico'), );
		}

		if( count( $options ) )
		{
			$form->get('formacao')->setValueOptions( $options );
		}
		else
		{
			$form->remove('formacao');
		}
		
		$request = $this->getRequest();
		
		if( $request->isPost() )
		{
			$post = $request->getPost();
			
			$form->setInputFilter( $this->getInputFilter() );
            $form->setData( $post );

            if( $form->isValid() )
			{
                $data = $form->getData();

				$this->sendEmail( $data );
				
				$this->flashMessenger()->addInfoMessage('Obrigado por nos contatar! Responderemos à você o mais rápido possível.');
				
				return $this->redirect()->toRoute('home');
            }
		}
		
		return new ViewModel(
			array(
				'form'	=> $form,
			)
		);
	}
	
	public function sendEmail( $data )
	{
		$translator = $this->getService('translator');
		
		$configuration = $this->getTable('Admin\Model\Configuration');
		
		$notificationEmail = $configuration->getRowByKey('key', 'email_notification')->value;
		
		$generator = $this->getEvent()->getRouteMatch()->getParam('controller', 'index') . '.' . $this->getEvent()->getRouteMatch()->getParam('action', 'index');

		$maildata['priority']	= 0; 
		$maildata['generator']	= $generator;
		$maildata['to_email']	= $notificationEmail ? $notificationEmail : 'tiago@catafesta.com';
		$maildata['subject']	= 'CMS - ' . $translator->translate('Novo contato pelo site');
		$maildata['format']		= 'html';

		$this->renderer = $this->getServiceLocator()->get('ViewRenderer');
		
		$body = $this->renderer->render(
			'mail/contato', 
			array(
					'nome'			=> $data['nome'],
					'email'			=> $data['email'],
					'telefone'		=> $data['telefone'],
					'cidade'		=> $data['cidade'],
					'local'			=> $data['local'],
					'data'			=> $data['data'],
					'tipo'			=> $data['tipo'],
					'conheceu'		=> $data['conheceu'],
					'horario'		=> $data['horario'],
					'mensagem'		=> $data['mensagem'],
					'formacao'		=> $data['formacao'],
					'siteName'		=> 'Catafesta Música para Eventos',
			)
		); 
		
		$maildata['body'] = $body;

		$this->genMail($maildata);
	}
	
	private function getInputFilter()
    {
        if( !$this->inputFilter )
		{
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
 
            $inputFilter->add($factory->createInput(array(
                'name'     => 'nome',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            )));
 
            $inputFilter->add($factory->createInput(array(
				'name'     => 'email',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'EmailAddress',
						'options' => array(
							'message' => 'Este e-mail é inválido',
						)
					),
				),
			)));
			
			$inputFilter->add($factory->createInput(array(
                'name'     => 'telefone',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            )));
			
			$inputFilter->add($factory->createInput(array(
                'name'     => 'cidade',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            )));
			
			$inputFilter->add($factory->createInput(array(
                'name'     => 'local',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            )));
			
			$inputFilter->add(
				$factory->createInput(
					array(
						'name'     => 'data',
						'required' => false,
						'filters'  => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
						),
						'validators' => array(
							array(
								'name'    => 'Date',
								'options' => array(
									'format' => 'd/m/Y',
								),
							),
						),
					)
				)
			);
			
			$inputFilter->add(
				$factory->createInput(
					array(
						'name'     => 'horario',
						'required' => false,
						'filters'  => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
						),
						'validators' => array(
							array(
								'name'    => 'Date',
								'options' => array(
									'format' => 'H:i',
								),
							),
						),
					)
				)
			);
			
			$inputFilter->add($factory->createInput(array(
				'name'     => 'mensagem',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
			)));
 
            $this->inputFilter = $inputFilter;
        }
 
        return $this->inputFilter;
    }
}