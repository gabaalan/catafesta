<?php
namespace Application\Controller;
 
use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
 
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Predicate\Predicate;

/**
 * Pagina inicial
 * 
 * @category Application
 * @package Controller
 * @author  Gaba
 */
class ProfissionaisController extends ActionController
{
	public function indexAction()
	{
		$sql = $this->getTable('Admin\Model\Profissional')->getSql();
		
		$select = $sql->select();
		$select
			->columns( array( 'id', 'nome', 'telefone', 'email', 'site', 'descricao' ) )
			->join(
				array( 'c' => 'categorias' ),
				'c.id = profissionais.categoria_id',
				array('categoria' => 'nome'),
				$select::JOIN_INNER
			)
			->order('c.nome ASC, profissionais.nome ASC')
			;
		
		$statement = $sql->prepareStatementForSqlObject( $select );
		
		$registros = $statement->execute();
		
		$profissas = array();
		
		foreach( $registros as $registro )
		{
			$profissas[ $registro['categoria'] ][] = $registro;
		}
		
		return new ViewModel(
			array(
				'registros'	=> $profissas,
			)
		);
	}
}