<?php
namespace Application\Controller;
 
use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;

/**
 * Pagina inicial
 * 
 * @category Application
 * @package Controller
 * @author  Gaba
 */
class RepertorioController extends ActionController
{
	public function indexAction()
	{
		$sql = $this->getTable('Admin\Model\Musica')->getSql();
		
		$select = $sql->select();
		$select
			->columns( array( 'id', 'nome', 'autor', 'tipo', 'caminho' ) )
			->join(
				array( 'c' => 'categorias' ),
				'c.id = musicas.categoria',
				array('categoria' => 'nome'),
				$select::JOIN_INNER
			)
			->order('c.nome ASC, musicas.id DESC')
			;
		
		$statement = $sql->prepareStatementForSqlObject( $select );
		
		$registros = $statement->execute();
		
		$musicas = array();
		
		foreach( $registros as $musica )
		{
			$musicas[ $musica['categoria'] ][] = $musica;
		}
		
		return new ViewModel(
			array(
				'registros'	=> $musicas,
			)
		);
	}
}