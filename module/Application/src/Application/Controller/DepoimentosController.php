<?php
namespace Application\Controller;
 
use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
 
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Predicate\Predicate;

use Zend\Db\Sql\Expression;

use Admin\Form\Depoimento as Form;
use Admin\Model\Depoimento as Model;

use Zend\Stdlib\ErrorHandler;

use PHPImageWorkshop\ImageWorkshop;

/**
 * Pagina inicial
 * 
 * @category Application
 * @package Controller
 * @author  Gaba
 */
class DepoimentosController extends ActionController
{
	public function enviarAction()
	{
		set_time_limit( 320 );
		ini_set('memory_limit', '-1');
		
		$form		= new Form(); //$this->getService('DbAdapter')

		$form->remove('aprovado');
		
		$back = $this->url()->fromRoute( 'depoimentos' );
		$form->get('reset')->setAttribute( 'onClick', "window.location='{$back}'" );
		
        $request	= $this->getRequest();
		
        if( $request->isPost() )
		{			
			$post		= $request->getPost();

            $register	= new Model();
			
			$filter		= $register->getInputFilter();
			
			$files		= $request->getFiles();
			
			if( $post->id && empty( $files->content['name'] ) )
				$filter->remove('foto');
			
			$filter->remove('aprovado');
			
            $form->setInputFilter( $filter );
            $form->setData( array_merge_recursive( $post->toArray(), $files->toArray() ) );
			
            if( $form->isValid() )
			{
                $data = $form->getData();

				$data['aprovado'] = null;
				
				if( !empty( $data['foto']['name'] ) )
				{
					$cutoff		= preg_replace( '/[^A-z0-9\-\_\.]/', '-', $data['foto']['name'] );
					
					$pathinfo	= pathinfo( $cutoff );

					$newname	= $pathinfo['filename'] . uniqid( '_' ) . '.' . $pathinfo['extension'];

					$dirname	= realpath( PUBLIC_PATH . '/uploads/depoimentos' );

					$path		= $dirname . '/' . $newname;
					
					ErrorHandler::start();
					move_uploaded_file( $data['foto']['tmp_name'], $path );
					ErrorHandler::stop(true);

					// ----------------------------------------------------------------------------------------
					// Gera os thumbs

					$imagem = ImageWorkshop::initFromPath( $path );

					$width	= 97;
					$height	= 97;

					$imagem->cropMaximumInPixel(0, 0, "MM");
					$imagem->resizeInPixel( $width, $height );

					$imagem->save( $dirname, 'thumb_' . $newname, true, null, 100 );

					// ----------------------------------------------------------------------------------------

					$data['foto'] = $newname;					
				}
				else
				{
					unset( $data['foto'] );
				}

				$register->setData( $data );

				$this->getTable('Admin\Model\Depoimento')->save( $register );
				
				$this->flashMessenger()->addInfoMessage('Depoimento enviado! Por favor aguarde a aprovação para visualizá-lo');

				return $this->redirect()->toRoute('depoimentos');
            }
        }

        return new ViewModel(
            array(
				'form' => $form
			)
        );
	}
	
	public function indexAction()
	{
		$form = new \Admin\Form\Depoimento();
		
		$sql = $this->getTable('Admin\Model\Depoimento')->getSql();
		
		$select = $sql->select();
		$select
			->columns( array( 'id', 'nome', 'email', 'mensagem', 'foto' ) )
			->where( array( 'aprovado', 1 ) )
			->order('id DESC')
			->limit(1)
			;
		
		$paginatorAdapter = new \Zend\Paginator\Adapter\DbSelect( $select, $sql );
		$paginator = new \Zend\Paginator\Paginator( $paginatorAdapter );
		$paginator->setItemCountPerPage( 200 ); //Default 10
		$paginator->setCurrentPageNumber( $this->params()->fromRoute('page') );
		
		return new ViewModel(
			array(
				'registros'	=> $paginator,
				'form'		=> $form,
				'gateway'	=> $this->getTable('Admin\Model\Depoimento'),
				'model'		=> new Model()
			)
		);
	}
}