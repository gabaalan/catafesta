<?php
namespace Application\Controller;
 
use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
 
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Predicate\Predicate;

/**
 * Pagina inicial
 * 
 * @category Application
 * @package Controller
 * @author  Gaba
 */
class VideosController extends ActionController
{
	public function indexAction()
	{
		$apikey = 'AIzaSyAgZvS-6hEiNXOa-ORLGBFYHR1ZlIi8hhQ';
		$playlistid = 'PLskO3Yw7pe4JXIp626OPPLkQKmelJqsYf';

		$client = new \Google_Client();
		$client->setDeveloperKey($apikey);
		$client->setScopes('https://www.googleapis.com/auth/youtube');
		$redirect = filter_var('http://catafesta.com', FILTER_SANITIZE_URL);
		$client->setRedirectUri($redirect);

		$youtube = new \Google_Service_YouTube($client);

		$playlistItemsResponse = $youtube->playlistItems->listPlaylistItems('snippet,status', array(
			'part' => 'snippet',
			'maxResults' => 50,
			//'pageToken' => 'CDIQAA',
			'playlistId' => $playlistid
		));

		$videos	= array();

		foreach( $playlistItemsResponse->getItems() as $video )
		{
			$snippet = $video->getSnippet();
			$thumbs = $snippet->getThumbnails();

			if( !$thumbs )
				continue;

			$videos[] = array(
				'id'		=> $snippet->getResourceId()->videoId,
				'titulo'	=> (string) $snippet->title,
				'thumbnail' => $snippet->getThumbnails()->getDefault()->url,
				'url'		=> 'https://www.youtube.com/watch?v=' . $snippet->getResourceId()->videoId
			);
		}

		//$usuario				= 'catafestame';
		//$youTube_UserFeedURL	= 'http://gdata.youtube.com/feeds/api/users/%s/uploads?orderby=updated&max-results=50&v=2.3';

		/*$cURL = curl_init( $youTube_UserFeedURL );

		$resultado = curl_exec( $cURL );

		curl_close($cURL);

		print_r($resultado);
		exit();
		
		$xml	= new \SimpleXMLElement($resultado);
		$videos	= array();
		
		foreach( $xml->entry AS $video )
		{
			$url = (string) $video->link['href'];
			
			parse_str( parse_url( $url, PHP_URL_QUERY ), $params );
			
			$id = $params['v'];
			
			$videos[] = array(
				'id'		=> $id,
				'titulo'	=> (string)$video->title,
				'thumbnail' => 'http://i' . rand(1, 4) .'.ytimg.com/vi/'. $id .'/1.jpg',
				'url'		=> $url
			);
		}*/
		
		return new ViewModel(
			array(
				'videos' => $videos
			)
		);
	}
}