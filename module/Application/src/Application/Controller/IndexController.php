<?php
namespace Application\Controller;
 
use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
 
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Predicate\Predicate;

/**
 * Pagina inicial
 * 
 * @category Application
 * @package Controller
 * @author  Gaba
 */
class IndexController extends ActionController
{	
	public function indexAction()
	{
		return new ViewModel(
			array(
				'musicas'		=> $this->getMusics(),
				'depoimentos'	=> $this->getTestimonials(),
				'fotos'			=> $this->getPhotos(),
                'banners'       => $this->getBanners()
			)
		);
	}

    private function getBanners()
    {
        $sql_banners = $this->getTable('Admin\Model\Banner')->getSql();

        $where = new \Zend\Db\Sql\Where();
        $where->greaterThanOrEqualTo( 'limite', new \Zend\Db\Sql\Expression( 'now()' ) );
        $where->or->isNull( 'limite' );

        $query_banners = $sql_banners->select();
        $query_banners
            ->where( $where )
            ->order('id DESC')
            ->limit(15)
        ;

        $banners = $sql_banners->prepareStatementForSqlObject( $query_banners )->execute();

        return $banners;
    }

	private function getMusics()
	{
		$sql = $this->getTable('Admin\Model\Musica')->getSql();
		
		$select = $sql->select();
		$select
			->columns( array( 'id', 'nome', 'caminho' ) )
			->order( new Expression('RAND()') )
			->limit(4)
			;
		
		$statement = $sql->prepareStatementForSqlObject( $select );
		
		$rand_musicas = new \Zend\Db\ResultSet\ResultSet();
		
		$rand_musicas->initialize( $statement->execute() );
		$rand_musicas->buffer();
		
		return $rand_musicas;
	}
	
	private function getTestimonials()
	{
		$sql = $this->getTable('Admin\Model\Depoimento')->getSql();
		
		$select = $sql->select();
		$select
			->columns( array( 'id', 'nome', 'email', 'mensagem', 'foto' ) )
			->where( array( 'aprovado', 1 ) )
			->order( new Expression('RAND()') )
			->limit(1)
			;
		
		$statement = $sql->prepareStatementForSqlObject( $select );
		
		return $statement->execute();
	}
	
	private function getPhotos()
	{
		$sql = $this->getTable('Admin\Model\Foto')->getSql();
		
		$select = $sql->select();
		$select
			->order( new Expression('RAND()') )
				->limit(12)
			;
		
		$statement = $sql->prepareStatementForSqlObject( $select );
		
		return $statement->execute();
	}
}