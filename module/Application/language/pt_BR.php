<?php

return array(
	'Home'		=> 'Home',
	'Logout'	=> 'Sair',
	'Login'		=> 'Entrar',
	'Date'		=> 'Data',
	'Options'	=> 'Opções',
	'Title'		=> 'Título',
	'Text'		=> 'Texto',
	'Username'	=> 'Usuário',
	'Password'	=> 'Senha',
	
	'Brazilian Portuguese'	=> 'Português Brasileiro',
	'EUA English'	=> 'Inglês dos Estados Unidos',
	'Page not found.'	=> 'Página não encontrada.',
	'An error occurred'	=> 'Desculpe, ocorreu um erro',
	
	"Developed by"		=> 'Desenvolvido por',
	"All rights reserved"		=> 'Todos os direitos reservados',
	"You was logged off"		=> 'Você foi desconectado',
	"Your session was started"	=> 'Sua sessão foi iniciada',
	"A record matching the input was found"	=> 'Um registro com este mesmo valor foi encontrado',
	"An error occurred during execution; please try again later."	=> 'Ocorreu um erro durante a execução, por favor, tente novamente mais tarde.',
	
	"Value is required and can't be empty"		=> 'O campo é obrigatório e não pode estar vazio',
);
