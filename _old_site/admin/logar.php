<?php

	session_start();
	
	$login = $_POST['logincata'];
	$senha = $_POST['passcata'];	
	$mdSenha = md5($senha);
	
	if(empty($login) or empty($senha)):
		echo "<script>alert('Campos incorretos');</script>";
        echo "<script>window.location='index.php';</script>";
		exit;
	endif;
	
	include "connect.php";
	$c->tabela = "usuarios";

	$search = $c->find("login", '=', $login);

	if($search["rows"] == 1) {
		$search = $c->objeto($search['query']);

		if(strcmp($mdSenha, $search->senha) == 0)
		{
			$_SESSION['cataLogin'] = $search->login;
			$_SESSION['cataSenha'] = $search->senha;
            echo "<script>window.location='main.php';</script>";
			exit;
		} else {
			echo "<script>alert('Senha invalida');</script>";
            echo "<script>window.location='index.php';</script>";
			exit;
		}
	} else {
		echo "<script>alert('Usuario incorreto');</script>";
        echo "<script>window.location='index.php';</script>";
		exit;
	}

?>