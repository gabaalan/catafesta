<?php

	/*
	+----------------------------------------------------------------+
		@Autor Gabriel Alan Pereira				
		@Data 30/11/2009
		
		Classe criada para manipulação de banco de dados com códigos
		menores, possibilitando o melhor entendimento de tal código
		e simplificando o mesmo.
		
		Esta classe em um padrão de programação, NÃO modifique-o.
	+----------------------------------------------------------------+
	*/

	class db
	{
		public $host, $db, $usuario, $senha, $tabela, $erro, $limite, $inicio, $ordem;
		private $conexao, $selectdb;
		
		//Função construtora, passar como parâmetros os dados do banco de dados, usuário e senha
		public function __construct($db, $host = "localhost", $usuario = "root", $senha = Null)
		{
			$this->db 			= $db;
			$this->host 		= $host;
			$this->usuario	= $usuario;
			$this->senha		= $senha;
			
			// Para não dar erro com a senha, quando estiver no localhost
			if($this->host == "localhost") 
			{
				$this->conexao = mysql_connect($this->host, $this->usuario, $this->senha) or die("Erro ao conectar: " . mysql_error());
			} else {
				$this->conexao = mysql_connect($this->host, $this->usuario, $this->senha) or die("Erro ao conectar: " . mysql_error());
			}
			
			$this->selectdb = mysql_select_db($this->db) or die("Erro ao selecionar banco de dados: " . mysql_error());
		}
		
		public function __get($propriedade)
		{
			return $propriedade;
		}
		
		public function __set($var, $valor)
		{
			$var = $valor;
		}
		
		//Funções para diminuir linha de código
		public function query($query) {
			return mysql_query($query);
		}
		
		public function rows($query) {
			return mysql_num_rows($query);
		}
		
		public function objeto($query)
		{
			return mysql_fetch_object($query);
		}
		
		//Procura todos os registros da tabela pré-determinada pelo usuário
		public function findAll()
		{
			if($this->tabela) 
			{
				$sql 		= "SELECT * FROM " . $this->tabela;
				$query 	= $this->query($sql, $this->conexao);
				$total		= $this->rows($query);
				
				return array("query" => $query, "rows" => $total);
			} else {
				$this->erro = "Não foi possível fazer a buscar pois a tabela não foi especificada";
				return $this->erro;
			}
		}
		
		//Procura registro específicos, retorna somente registros restritos ao valor
		public function find($campo, $condicao, $valor)
		{
			//Os if's abaixo verificam se as variaveis existem, e caso existam cria uma query diferente para cada um
			if($this->tabela) 
			{
				$ordem = ($this->ordem) ? $this->ordem : "";
				$limite = ($this->limite) ? " LIMIT " . $this->limite : "";
				$inicio = ($this->inicio) ? "," . $this->inicio : "";
				$condicao = ($condicao) ? $condicao : "=";
				
				$sql = "SELECT * FROM " . $this->tabela . " WHERE " . $campo . " ". $condicao ." '" . $valor . "' ORDER BY " . $campo . " " . $ordem . $limite . $inicio;
				
				$query = $this->query($sql);
				$total = $this->rows($query);

				return array("query" => $query, "rows" => $total);
			} else {
				$this->erro = "Não foi possível fazer a buscar pois a tabela não foi especificada";
				return $this->erro;
			}
		}
		
		//Procura registros específicos, retorna registros parecidos com o valor, usa 'LIKE' no sql em vez de '='
		public function openFind($campo, $valor)
		{
			//Os if's abaixo verificam se as variaveis existem, e caso existam cria uma query diferente para cada um
			if($this->tabela) 
			{
				$ordem = ($this->ordem) ? $this->ordem : "";
				$limite = ($this->limite) ? " LIMIT " . $this->limite . "," : "";
				$inicio = ($this->inicio) ? "," . $this->inicio : "";
				
				$sql = "SELECT * FROM " . $this->tabela . " WHERE " . $campo . " LIKE '%" . $valor . "%' ORDER BY " . $campo . " " . $ordem . $limite . $inicio;
								
				$query = $this->query($sql);
				$total = $this->rows($query);
				
				return array("query" => $query, "rows" => $total);
			} else {
				$this->erro = "Não foi possível fazer a buscar pois a tabela não foi especificada";
				return $this->erro;
			}
		}
		
		// Função para Inserir, passe os campos e os valores. Ex: insert("campo1, campo2", "valor1, valor2")
		public function insert($campos, $valores)
		{
			if($this->tabela)
			{
				if((strlen($campos) >= 2) and (strlen($valores) >= 2))
				{
					$exp = explode(",", $valores);

					$limit = count($exp);
					$cont = 0;

					$sql = "INSERT INTO " . $this->tabela . " (". $campos .") VALUES (";
					foreach($exp as $valor) 
					{
						$sql .= "'" . trim($valor) . "'";
						$cont++;
						if($cont < $limit) 
						{
							$sql .= ",";
						}
					}
					$sql .= ")";

					$this->query($sql) or die("Erro ao inserir: " . mysql_error());
				} else {
					$this->erro = "Não foi possível inserir os dados pois não foram passados parametros corretos";
					return $this->erro;
				}
			} else {
				$this->erro = "Não foi possível inserir os dados pois a tabela não foi especificada";
				return $this->erro;
			}
		}
		
		//Função para deletar registros, passe o campo e seu valor como condição para a remoção
		public function delete($campo, $valor)
		{
			if($this->tabela)
			{
				if(($campo) and ($valor))
				{
					$sql = "DELETE FROM " . $this->tabela .  " WHERE " . $campo . " = '" . $valor . "'";
					
					$query = $this->query($sql) or die("Erro ao deletar: " . mysql_error());					
				} else {
					$this->erro = "Não foi possível deletar pois não foram passados parametros válidos";
					return $this->erro;
				}
			} else {
				$this->erro = "Não foi possível deletar os dados pois a tabela não foi especificada";
				return $this->erro;
			}
		}
		
		//Função para Updade: Passe os valores EX: ("Campos a serem atualizados", "Valores dos campos", "campos de condições", "valores das condições")
		public function update($setFields, $setValues, $conditions, $values)
		{
			if($this->tabela)
			{
				if(($setFields) and ($setValues) and ($conditions) and ($values))
				{
					$sql = "UPDATE ". $this->tabela ." SET ";
					
					$eSF = explode(',', $setFields);
					$eSV = explode(',', $setValues);
					$eCo = explode(',', $conditions);
					$eVa = explode(',', $values);
					
					$eSFc = count($eSF);
					$eSVc = count($eSV);
					$eCoc = count($eCo);
					$eVac = count($eVa);					
					
					$i = 0;					
					foreach($eSF as $valor)
					{
						$sql .= $valor . " = '" . trim($eSV[$i]) . "'";
						$i++;
						if($i < $eSFc) { $sql .= ","; }
					}
					
					$sql .= " WHERE ";
					
					$i = 0;
					foreach($eCo as $valor)
					{
						$sql .= $valor . " = '" . trim($eVa[$i]) . "'";
						$i++;
						if($i < $eCoc) { $sql .= " AND "; }
					}
					
					$this->query($sql) or die("Erro ao atualizar: " . mysql_error());	
				} else {
					$this->erro = "Não foi possível atualizar, pois os campos nao foram passados corretamente";
					return $this->erro;
				}
			} else {
				$this->erro = "Não foi possível atualizar os dados pois a tabela não foi especificada";
				return $this->erro;
			}
		}
	}//Fim da Classe
	
?>