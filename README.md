Catafesta.com
=======================

Installation
------------

Using Composer (recommended)
----------------------------
The recommended way to get a working copy of this project is to clone the repository
and use composer to install dependencies:

    cd dir/projetos
    git clone https://github.com/gabrielalan/catafesta.git Catafesta
    cd Catafesta
    php composer.phar update

Remember to rename the file "config/autoload/local.php.dist" to "local.php" for 
the system recognize it. It contains part of the database configuration.

Virtual Host
------------

	NameVirtualHost catafesta.com:80

	<Directory "dir/projetos/Catafesta/public">
		Options All
		AllowOverride All
		Order allow,deny
		Allow from all
	</Directory>

	<VirtualHost catafesta.com:80>
		ServerName catafesta.com
		ServerAlias www.catafesta.com
		DocumentRoot "dir/projetos/Catafesta/public"
	</VirtualHost>

