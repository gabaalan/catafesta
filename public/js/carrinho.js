$(function() {
	
	$('.carrinho table').on( 'itemsChange', updateCart );
	
	$('.carrinho table tbody').on( 'click', 'tr td .remove', removeProduct );
	
	$('.carrinho table tbody').on( 'change', 'tr td.quantity input', changeQuantity );
	$('.carrinho table tbody').on( 'keyup', 'tr td.quantity input', changeQuantity );
	$('.carrinho table tbody').on( 'keydown', 'tr td.quantity input', changeQuantity );
	$('.carrinho table tbody').on( 'keypress', 'tr td.quantity input', enterFalse );
	
	$('.product').on( 'click', wantAddProduct );
	
});

function enterFalse( evento )
{
	if( evento.which == 13 ) 
	{
		evento.stopPropagation();
		evento.preventDefault();
		return false;
	}
	
	$(this).trigger( 'change' );
}

function changeQuantity( evento )
{
	var row = $(this).parent().parent();
	
	var quantity = $(this).val();

	var value	= parseFloat( $(row).data('product-value') );
	var percent	= parseFloat( $(row).data('product-percent') );
	
	var total = value * quantity;
	var tax = ( percent / 100 ) * total;
	
	row.find('.total').text( 'R$' + $.number( total, 2, ',', '.' ) );
	row.find('.tax').text( 'R$' + $.number( tax, 2, ',', '.' ) );
	
	$('.carrinho table').trigger( 'itemsChange' );
}

function removeProduct( evento )
{
	evento.stopPropagation();
	evento.preventDefault();
	
	$(this).parent().parent().remove();
	
	$('.carrinho table').trigger( 'itemsChange' );
	
	return false;
}

function wantAddProduct( evento )
{
	evento.stopPropagation();
	evento.preventDefault();
	
	var id		= $(this).data('product-id');
	var name	= $(this).data('product-name');
	var value	= $(this).data('product-value');
	var percent	= $(this).data('product-percent');
	
	addToCart( id, name, value, percent );
	
	return false;
}

function addToCart( id, name, value, percent )
{
	var structure = 
		'<tr>'
		+ '<td class="name vertical-middle"></td>'
		+ '<td class="text-right vertical-middle value"></td>'
		+ '<td class="text-center vertical-middle quantity"><input type="text" name="products[]" class="text-center no-right-padding no-left-padding form-control input-sm" value="" /></td>'
		+ '<td class="text-right vertical-middle total"></td>'
		+ '<td class="text-right vertical-middle tax"></td>'
		+ '<td class="text-center vertical-middle id">'
			+ '<input type="hidden" name="products[]" value="" />'
			+ '<a href="" class="btn btn-danger btn-xs remove"><span class="glyphicon glyphicon-remove"></span></a>'
		+ '</td>'
		+ '</tr>'
	;
	
	var trId = 'product' + id;
	
	var exist = $('.carrinho table tbody').find('tr#' + trId);
	
	if( exist.length > 0 )
	{
		var quantity = parseFloat( exist.find('.quantity input').val() ) + 1;

		var row = exist;
	}
	else
	{
		var quantity = 1;

		var row = $( structure );
	}
	
	var total = value * quantity;
	
	var tax = ( percent / 100 ) * total;
	
	row.find('.name').text( name );
	
	row.find('.value').text( 'R$' + $.number( value, 2, ',', '.' ) );
	row.find('.total').text( 'R$' + $.number( total, 2, ',', '.' ) );
	row.find('.tax').text( 'R$' + $.number( tax, 2, ',', '.' ) );
	
	row.find('.id input').attr('name', 'products[' + id + '][id]').val( id );
	row.find('.quantity input').attr('name', 'products[' + id + '][quantity]').val( quantity );
	
	row.attr('id', trId);
	
	row.data('product-id', id);
	row.data('product-name', name);
	row.data('product-value', value);
	row.data('product-percent', percent);
	
	if( exist.length <= 0 )
	{
		$('.carrinho table tbody').append( row );
	}
	
	$('.carrinho table').trigger( 'itemsChange' );
}

function updateCart( evento )
{
	var rows = $('.carrinho table tbody tr');
	
	if( rows.length <= 0 )
	{
		$('.carrinho *[type=submit]').attr('disabled', 'disabled').addClass('disabled');
		
		$('.carrinho table tfoot .price-total').text( '---' );
		$('.carrinho table tfoot .tax-total').text( '---' );
		$('.carrinho table tfoot .total').text( '---' );
		
		return false;
	}
	
	$('.carrinho *[type=submit]').attr('disabled', null).removeClass('disabled');
	
	var price_total = 0;
	var tax_total = 0;
	var total = 0;
	
	$.each(
		rows,
		function( index, row )
		{
			var value		= $(row).data('product-value');
			var percent		= $(row).data('product-percent');
			var quantity	= $(row).find('.quantity input').val();
			
			var total	= value * quantity;
			var tax		= ( percent / 100 ) * total;
			
			price_total += total;
			tax_total	+= tax;
		}
	);
		
	total = price_total + tax_total;
		
	$('.carrinho table tfoot .price-total').text( 'R$' + $.number( price_total, 2, ',', '.' ) );
	$('.carrinho table tfoot .tax-total').text( 'R$' + $.number( tax_total, 2, ',', '.' ) );
	$('.carrinho table tfoot .total').text( 'R$' + $.number( total, 2, ',', '.' ) );
}