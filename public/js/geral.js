var nomesdias = [ 
	('Domingo'), 
	('Segunda'), 
	('Terça'), 
	('Quarta'), 
	('Quinta'), 
	('Sexta'), 
	('Sábado') 
];

var nomesPequenosDias = ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb'];

var nomesMeses = ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'];

var nomesPequenosMeses = ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'];

var audio = null;

$(function() {  
	$('input[rel=popover], .show-popover').popover();
	$('input[rel=tooltip], .show-tooltip, span[rel=tooltip], a[rel=tooltip]').tooltip();

	$('.date')
		.mask( "99/99/9999" )
		.datepicker(
			{
				dateFormat: 'dd/mm/yy',
				dayNames: nomesdias,
				dayNamesMin: nomesPequenosDias,
				monthNames: nomesMeses,
				monthNamesShort: nomesPequenosMeses,

				showOtherMonths: true,
				selectOtherMonths: true,
				buttonImage     : 'images/calendar.png',
				changeMonth     : true,
				changeYear      : true
			}
		)
	;
	
	$('.phone').mask( "(99) 9999-9999?9" );
	$('.hour').mask( "99:99" );
	
	$('.fancybox').fancybox(
		{
			padding: 0
		}
	);
		
	$('.fancybox-media').fancybox(
		{
			openEffect  : 'none',
			closeEffect : 'none',
			padding		: 0,
			helpers: 
			{
				media : {}
			}
		}
	);
		
	$('.popover').popover();
	$('.html-popover').popover( { html: true } );
		
	// Audio
	audio = audiojs.create(
		$('.music-radio audio').get(0),
		{
			css				: false,
			createPlayer	: 
			{
				markup				: false,
				playPauseClass		: 'play-pause',
				scrubberClass		: 'scrubber',
				progressClass		: 'progress',
				loaderClass			: 'loaded',
				timeClass			: 'time',
				durationClass		: 'duration',
				playedClass			: 'played',
				errorMessageClass	: 'error-message',
				playingClass		: 'playing',
				loadingClass		: 'loading',
				errorClass			: 'error'
			},
			trackEnded	: function() 
			{
				nextMusic( null );
			}
		}
	);
	
	var first = $('.music-radio ul li').first();
	
	audio.load( first.find('a').data('src') );
	
	first.addClass('playing');
	
	$('.play-radio').on( 'click', playOnRadio );
	$('.radio-wrapper .toggle').on( 'click', toggleRadio );
	$('.radio-wrapper .player .next').on( 'click', nextMusic );
	$('.radio-wrapper .player .prev').on( 'click', prevMusic );
});

function prevMusic( evento )
{
	if( evento )
		evento.preventDefault();
	
	var next = $('.music-radio ul li.playing').prev();
				
	if( !next.length )
		next = $('.music-radio ul li').last();

	next.addClass('playing').siblings().removeClass('playing');

	audio.load( $('a', next).data('src') );
	audio.play();
	
	return false;
}

function nextMusic( evento )
{
	if( evento )
		evento.preventDefault();
	
	var next = $('.music-radio ul li.playing').next();
				
	if( !next.length )
		next = $('.music-radio ul li').first();

	next.addClass('playing').siblings().removeClass('playing');

	audio.load( $('a', next).data('src') );
	audio.play();
	
	return false;
}

function toggleRadio( evento )
{
	if( evento )
		evento.preventDefault();
	
	var wrapper = $('.radio-wrapper');

	if( wrapper.hasClass('show') )
	{		
		closeRadio();
	}
	else
	{
		openRadio();
	}
	
	return false;
}

function closeRadio()
{
	$('.radio-wrapper .toggle').find('.glyphicon').removeClass( 'glyphicon-chevron-up' ).addClass( 'glyphicon-chevron-down' );
	
	$('.radio-wrapper').removeClass('show');
}

function openRadio()
{
	$('.radio-wrapper .toggle').find('.glyphicon').removeClass( 'glyphicon-chevron-down' ).addClass( 'glyphicon-chevron-up' );
	
	$('.radio-wrapper').addClass('show');
}

function playOnRadio( evento )
{
	evento.preventDefault();
	
	$('.music-radio .player').removeClass('error');
	
	var src = $(this).data('src');
	
	if( src && audio )
	{
		audio.load( src );
		audio.play();
		
		//$('.radio-wrapper').addClass('show');
		openRadio();
		
		$('.music-radio ul li.playing').removeClass('playing');
		
		if( $(this).hasClass('radio-list') )
		{
			$(this).parent().addClass('playing');
		}
		else
		{
			var onList = $('.music-radio ul li a[data-src="' + src + '"]');

			if( onList.length )
				$(onList.get(0)).parent().addClass('playing');
		}
	}
	
	return false;
}