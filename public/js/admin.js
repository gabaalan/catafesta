var nomesdias = [ 
	('Domingo'), 
	('Segunda'), 
	('Terça'), 
	('Quarta'), 
	('Quinta'), 
	('Sexta'), 
	('Sábado') 
];

var nomesPequenosDias = ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb'];

var nomesMeses = ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'];

var nomesPequenosMeses = ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'];

var audio = null;

$(function() {  
	$('input[rel=popover], .show-popover').popover();
	$('input[rel=tooltip], .show-tooltip, span[rel=tooltip], a[rel=tooltip]').tooltip();

	$('.date')
		.mask( "99/99/9999" )
		.datepicker(
			{
				dateFormat: 'dd/mm/yy',
				dayNames: nomesdias,
				dayNamesMin: nomesPequenosDias,
				monthNames: nomesMeses,
				monthNamesShort: nomesPequenosMeses,

				showOtherMonths: true,
				selectOtherMonths: true,
				buttonImage     : 'images/calendar.png',
				changeMonth     : true,
				changeYear      : true
			}
		)
	;

    $('.hour').mask( "99:99" );
	
	$('.fancybox').fancybox(
		{
			padding: 0
		}
	);
		
	$('.fancybox-media').fancybox(
		{
			openEffect  : 'none',
			closeEffect : 'none',
			padding		: 0,
			helpers: 
			{
				media : {}
			}
		}
	);
	
	$('input.checkall').bind( 'click', checkAll );
	$('.submit-main').bind( 'click', submitMain );
	
	CKEDITOR.config.allowedContent = true; 
	CKEDITOR.disableAutoInline = true;
	
	if( $('textarea[name=mensagem]').length > 0 )
		CKEDITOR.replace( 'mensagem' );	
	
	if( $('textarea[name=description]').length > 0 )
		CKEDITOR.replace( 'description' );	
	
	if( $('.value-ck').length > 0 )
		CKEDITOR.replace( 'value' );	
});

function submitMain( evento )
{
	if( $(this).hasClass('confirm-remove') )
	{
		var confirmar = confirm( 'Deseja realmente excluir este(s) registro(s)? Este processo é irreversível' );

		if( !confirmar )
		{
			evento.preventDefault();
			evento.stopPropagation();

			return false;
		}
	}
	
	$('form.main').submit();
}

function checkAll( evento )
{
	var checked = $(this).is(':checked');

	var name	= $(this).attr('name');

		name	= 'input.checkbyall';

	var checks = $( name );

	if( checked )
	{
		$(checks).not(':checked').trigger( 'click' );

		$(checks).attr( 'checked', 'checked' );
	}
	else
	{               
		$(checks, ':checked').trigger( 'click' );

		$(checks).attr( 'checked', null );
	}
}